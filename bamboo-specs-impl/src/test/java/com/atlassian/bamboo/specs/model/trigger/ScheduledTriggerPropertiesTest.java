package com.atlassian.bamboo.specs.model.trigger;

import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.builders.trigger.ScheduledTrigger;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

public class ScheduledTriggerPropertiesTest {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private static final String NAME = "name";

    @Test
    public void testSuccessfulCreation() throws Exception {
        ScheduledTriggerProperties scheduledTrigger = EntityPropertiesBuilders.build(scheduledTrigger()
                .cronExpression("0 0 0 ? * *"));

        assertThat(scheduledTrigger.getCronExpression(), equalTo("0 0 0 ? * *"));
    }

    @Test
    public void testInvalidCronExpression() throws Exception {
        expectedException.expect(PropertiesValidationException.class);

        ScheduledTriggerProperties scheduledTrigger = EntityPropertiesBuilders.build(scheduledTrigger()
                .cronExpression("not valid cron expression"));

    }

    @Test
    public void testScheduleEvery() throws Exception {
        ScheduledTriggerProperties scheduledTrigger = EntityPropertiesBuilders.build(scheduledTrigger()
                .scheduleEvery(30, TimeUnit.SECONDS));

        assertThat(scheduledTrigger.getCronExpression(), equalTo("0/30 * * ? * *"));
    }

    @Test
    public void testScheduleOnceDaily() throws Exception {
        ScheduledTriggerProperties scheduledTrigger = EntityPropertiesBuilders.build(scheduledTrigger()
                .scheduleOnceDaily(LocalTime.of(15, 13)));

        assertThat(scheduledTrigger.getCronExpression(), equalTo("0 13 15 ? * *"));
    }

    @Test
    public void testScheduleWeekly() throws Exception {
        ScheduledTrigger triggerBuilder = scheduledTrigger();

        ScheduledTriggerProperties scheduledTrigger = EntityPropertiesBuilders.build(triggerBuilder
                .scheduleWeekly(LocalTime.of(14, 13), DayOfWeek.SUNDAY));

        assertThat(scheduledTrigger.getCronExpression(), equalTo("0 13 14 ? * 1"));
    }

    @Test
    public void testScheduleWeeklyMoreDays() throws Exception {
        ScheduledTrigger triggerBuilder = scheduledTrigger();

        ScheduledTriggerProperties scheduledTrigger = EntityPropertiesBuilders.build(triggerBuilder
                .scheduleWeekly(LocalTime.of(8, 8), DayOfWeek.FRIDAY, DayOfWeek.FRIDAY, DayOfWeek.SATURDAY, DayOfWeek.WEDNESDAY));

        assertThat(scheduledTrigger.getCronExpression(), equalTo("0 8 8 ? * 6,6,7,4"));
    }

    @Test
    public void testScheduleMonthly() throws Exception {
        ScheduledTriggerProperties scheduledTrigger = EntityPropertiesBuilders.build(scheduledTrigger()
                .scheduleMonthly(LocalTime.of(13, 0), 11));

        assertThat(scheduledTrigger.getCronExpression(), equalTo("0 0 13 11 * ?"));
    }

    private ScheduledTrigger scheduledTrigger() {
        return new ScheduledTrigger().name(NAME);
    }
}
