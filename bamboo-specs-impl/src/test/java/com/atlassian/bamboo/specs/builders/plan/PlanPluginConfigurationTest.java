package com.atlassian.bamboo.specs.builders.plan;

import com.atlassian.bamboo.specs.api.builders.plan.Plan;
import com.atlassian.bamboo.specs.api.builders.plan.configuration.AllOtherPluginsConfiguration;
import com.atlassian.bamboo.specs.api.builders.project.Project;
import com.atlassian.bamboo.specs.api.model.plan.PlanProperties;
import com.atlassian.bamboo.specs.api.model.plan.configuration.AllOtherPluginsConfigurationProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.util.MapBuilder;
import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class PlanPluginConfigurationTest {

    @Test
    public void testAllOtherPluginConfigurationIsAdditive() {
        final Project project = new Project().key("BAR");
        final Plan plan = new Plan(project, "foo", "FOO")
                .pluginConfigurations(
                        new AllOtherPluginsConfiguration().configuration(new MapBuilder<String, Object>().put("a", "a").build()),
                        new AllOtherPluginsConfiguration().configuration(new MapBuilder<String, Object>().put("b", "b").build()),
                        new AllOtherPluginsConfiguration().configuration(new MapBuilder<String, Object>().put("c", "c").build()),
                        new AllOtherPluginsConfiguration().configuration(new MapBuilder<String, Object>().put("c", "d").build()));


        PlanProperties planProperties = EntityPropertiesBuilders.build(plan);

        assertEquals(1, planProperties.getPluginConfigurations().size());
        AllOtherPluginsConfigurationProperties aopcp = (AllOtherPluginsConfigurationProperties) planProperties.getPluginConfigurations().get(0);
        assertEquals("a", aopcp.getConfiguration().get("a"));
        assertEquals("b", aopcp.getConfiguration().get("b"));
        assertEquals("d", aopcp.getConfiguration().get("c"));

    }

    @Test
    public void testAllOtherPluginConfigurationIsAdditiveWhenUsingNestedMaps() {
        final Project project = new Project().key("BAR");
        final Plan plan = new Plan(project, "foo", "FOO")
                .pluginConfigurations(
                        new AllOtherPluginsConfiguration().configuration(new MapBuilder<String, Object>().put("custom", new MapBuilder<String, Object>().put("a", "a").build()).build()),
                        new AllOtherPluginsConfiguration().configuration(new MapBuilder<String, Object>().put("custom", new MapBuilder<String, Object>().put("b", "b").build()).build()),
                        new AllOtherPluginsConfiguration().configuration(new MapBuilder<String, Object>().put("custom", new MapBuilder<String, Object>().put("c", "c").build()).build()),
                        new AllOtherPluginsConfiguration().configuration(new MapBuilder<String, Object>().put("custom", new MapBuilder<String, Object>().put("c", "d").build()).build()));


        PlanProperties planProperties = EntityPropertiesBuilders.build(plan);

        assertEquals(1, planProperties.getPluginConfigurations().size());
        AllOtherPluginsConfigurationProperties aopcp = (AllOtherPluginsConfigurationProperties) planProperties.getPluginConfigurations().get(0);
        Map<String, Object> customMap = (Map<String, Object>) aopcp.getConfiguration().get("custom");
        assertEquals("a", customMap.get("a"));
        assertEquals("b", customMap.get("b"));
        assertEquals("d", customMap.get("c"));

    }

    @Test
    public void testAllOtherPluginConfigurationCanBeClearedByInsertingNull() {
        final Project project = new Project().key("BAR");
        final Plan plan = new Plan(project, "foo", "FOO")
                .pluginConfigurations(
                        new AllOtherPluginsConfiguration().configuration(new MapBuilder<String, Object>().put("custom", new MapBuilder<String, Object>().put("a", "a").build()).build()),
                        new AllOtherPluginsConfiguration().configuration(new MapBuilder<String, Object>().put("custom.uuk", "uuk").build()),
                        new AllOtherPluginsConfiguration().configuration(new MapBuilder<String, Object>().put("custom", null).build()),
                        new AllOtherPluginsConfiguration().configuration(new MapBuilder<String, Object>().put("custom", new MapBuilder<String, Object>().put("c", "c").build()).build()),
                        new AllOtherPluginsConfiguration().configuration(new MapBuilder<String, Object>().put("custom", new MapBuilder<String, Object>().put("c", "d").build()).build()));


        PlanProperties planProperties = EntityPropertiesBuilders.build(plan);

        assertEquals(1, planProperties.getPluginConfigurations().size());
        AllOtherPluginsConfigurationProperties aopcp = (AllOtherPluginsConfigurationProperties) planProperties.getPluginConfigurations().get(0);
        Map<String, Object> customMap = (Map<String, Object>) aopcp.getConfiguration().get("custom");
        assertFalse(customMap.containsKey("a"));
        assertFalse(aopcp.getConfiguration().containsKey("custom.uuk"));
        assertEquals("d", customMap.get("c"));

    }

}

