package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.model.task.InjectVariablesScope;
import com.atlassian.bamboo.specs.model.task.InjectVariablesTaskProperties;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class InjectVariablesTaskTest {
    final String path = "path";
    final String namespace = "namespace";
    final InjectVariablesScope scope = InjectVariablesScope.RESULT;

    @Test
    public void testBuilderOK() {

        final InjectVariablesTask injectVariablesBuilder = new InjectVariablesTask()
                .path(path)
                .namespace(namespace)
                .scope(scope);

        final InjectVariablesTaskProperties properties = injectVariablesBuilder.build();

        assertThat(properties.getPath(), is(path));
        assertThat(properties.getNamespace(), is(namespace));
        assertThat(properties.getScope(), is(scope));
        assertThat(properties.getAtlassianPlugin().getCompleteModuleKey(), is("com.atlassian.bamboo.plugins.bamboo-variable-inject-plugin:inject"));
    }

    @Test
    public void testBuilderDefaults() {

        final InjectVariablesTask injectVariablesBuilder = new InjectVariablesTask()
                .path(path);

        final InjectVariablesTaskProperties properties = injectVariablesBuilder.build();

        assertThat(properties.getPath(), is(path));
        assertThat(properties.getNamespace(), is("inject"));
        assertThat(properties.getScope(), is(InjectVariablesScope.LOCAL));
    }

    @Test
    public void testBuilderLocalScopeShortcut() {

        final InjectVariablesTask injectVariablesBuilder = new InjectVariablesTask()
                .path(path)
                .scopeLocal();

        final InjectVariablesTaskProperties properties = injectVariablesBuilder.build();

        assertThat(properties.getScope(), is(InjectVariablesScope.LOCAL));
    }

    @Test
    public void testBuilderResultScopeShortcut() {

        final InjectVariablesTask injectVariablesBuilder = new InjectVariablesTask()
                .path(path)
                .scopeResult();

        final InjectVariablesTaskProperties properties = injectVariablesBuilder.build();

        assertThat(properties.getScope(), is(InjectVariablesScope.RESULT));
    }

    @Test(expected = PropertiesValidationException.class)
    public void testThrowsExceptionCreatedWithEmptyPath() {
        new InjectVariablesTask()
            .build();
    }

    @Test(expected = PropertiesValidationException.class)
    public void testThrowsExceptionOverridenWithEmptyPath() {
        new InjectVariablesTask()
                .path("");
    }

    @Test(expected = PropertiesValidationException.class)
    public void testThrowsExceptionWithEmptyNamespace() {

        new InjectVariablesTask()
                .path(path)
                .namespace("");
    }

    @Test(expected = PropertiesValidationException.class)
    public void testThrowsExceptionWithInvalidNamespace() {

        new InjectVariablesTask()
                .path(path)
                .namespace("!@#$%^")
                .build();
    }

    @Test(expected = PropertiesValidationException.class)
    public void testThrowsExceptionWithNullScope() {

        new InjectVariablesTask()
                .path(path)
                .scope(null);
    }

}