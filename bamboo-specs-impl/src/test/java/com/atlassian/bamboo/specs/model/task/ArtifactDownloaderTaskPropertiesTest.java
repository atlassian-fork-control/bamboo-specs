package com.atlassian.bamboo.specs.model.task;

import com.atlassian.bamboo.specs.api.builders.plan.PlanIdentifier;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.builders.task.ArtifactDownloaderTask;
import com.atlassian.bamboo.specs.builders.task.DownloadItem;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ArtifactDownloaderTaskPropertiesTest {
    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    @Test
    public void creatingDownloaderTaskWithoutDownload() throws Exception {
        expectedException.expect(PropertiesValidationException.class);
        EntityPropertiesBuilders.build(new ArtifactDownloaderTask().sourcePlan(new PlanIdentifier("PA", "DU")));
    }

    @Test
    public void creatingDownloaderTaskWithoutSourcePlan() throws Exception {
        EntityPropertiesBuilders.build(new ArtifactDownloaderTask().artifacts(new DownloadItem().allArtifacts(true)));
    }

    @Test
    public void creatingDownloaderTaskWithFaultyArtifactIdentifier() throws Exception {
        expectedException.expect(PropertiesValidationException.class);
        EntityPropertiesBuilders.build(new ArtifactDownloaderTask().sourcePlan(new PlanIdentifier("PA", "DU"))
                .artifacts(new DownloadItem().artifact(null)));
    }

    @Test
    public void testSuccessfulCreation() throws Exception {
        ArtifactDownloaderTaskProperties properties = EntityPropertiesBuilders.build(new ArtifactDownloaderTask().sourcePlan(new PlanIdentifier("PA", "DU"))
                .artifacts(
                        new DownloadItem().allArtifacts(true),
                        new DownloadItem().path("blah").artifact("blah blah blah"))
        );

        assertEquals(2, properties.getArtifacts().size());
        assertTrue(properties.getArtifacts().get(0).isAllArtifacts());
        assertFalse(properties.getArtifacts().get(1).isAllArtifacts());
        assertEquals("blah", properties.getArtifacts().get(1).getPath());
        assertEquals("blah blah blah", properties.getArtifacts().get(1).getArtifactName());
    }
}
