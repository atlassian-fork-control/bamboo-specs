package com.atlassian.bamboo.specs.model.task;


import com.atlassian.bamboo.specs.api.builders.repository.VcsRepositoryIdentifier;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.builders.task.CheckoutItem;
import com.atlassian.bamboo.specs.builders.task.VcsCheckoutTask;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class VcsCheckoutTaskPropertiesTest {
    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    @Test
    public void creatingCheckoutTaskWithoutCheckoutItem() throws Exception {
        expectedException.expect(PropertiesValidationException.class);
        EntityPropertiesBuilders.build(new VcsCheckoutTask());
    }

    @Test
    public void creatingCheckoutTaskWithDuplicatedPath() throws Exception {
        expectedException.expect(PropertiesValidationException.class);
        EntityPropertiesBuilders.build(new VcsCheckoutTask()
                .checkoutItems(
                        new CheckoutItem().repository(new VcsRepositoryIdentifier().name("blah")).path("path"),
                        new CheckoutItem().repository(new VcsRepositoryIdentifier().name("blah blah")).path("path")));
    }

    @Test
    public void creatingCheckoutTaskWithDuplicatedEmptyPath() throws Exception {
        expectedException.expect(PropertiesValidationException.class);
        EntityPropertiesBuilders.build(new VcsCheckoutTask()
                .checkoutItems(
                        new CheckoutItem().repository(new VcsRepositoryIdentifier().name("blah")),
                        new CheckoutItem().repository(new VcsRepositoryIdentifier().name("blah blah"))));
    }

    @Test
    public void creatingCheckoutTaskWithDuplicatedRepo() throws Exception {
        expectedException.expect(PropertiesValidationException.class);
        EntityPropertiesBuilders.build(new VcsCheckoutTask()
                .checkoutItems(
                        new CheckoutItem().repository(new VcsRepositoryIdentifier().name("blah")).path("path"),
                        new CheckoutItem().repository(new VcsRepositoryIdentifier().name("blah"))));
    }

    @Test
    public void creatingCheckoutTaskWithDefaultRepoUsedTwice() throws Exception {
        expectedException.expect(PropertiesValidationException.class);
        EntityPropertiesBuilders.build(new VcsCheckoutTask()
                .checkoutItems(
                        new CheckoutItem().defaultRepository().path("path"),
                        new CheckoutItem().defaultRepository()));
    }

    @Test
    public void testSuccessfulCreation() throws Exception {
        VcsCheckoutTaskProperties properties = EntityPropertiesBuilders.build(new VcsCheckoutTask()
                .checkoutItems(
                        new CheckoutItem().repository("blah"),
                        new CheckoutItem().repository(new VcsRepositoryIdentifier().name("blah blah")).path("path"),
                        new CheckoutItem().defaultRepository().path("dddddd")));

        assertEquals(3, properties.getCheckoutItems().size());
        assertEquals("blah blah", properties.getCheckoutItems().get(1).getRepository().getName());
        assertEquals("", properties.getCheckoutItems().get(0).getPath());
        assertEquals("dddddd", properties.getCheckoutItems().get(2).getPath());
        assertTrue(properties.getCheckoutItems().get(2).isDefaultRepository());
    }
}
