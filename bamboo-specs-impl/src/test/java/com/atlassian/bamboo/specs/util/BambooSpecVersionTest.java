package com.atlassian.bamboo.specs.util;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BambooSpecVersionTest {

    @Test
    public void testBambooSpecVersionReadsThePropertyFile() throws Exception {
        String projectVersion = System.getProperty("project.version");

        String modelVersion = BambooSpecVersion.getModelVersion();

        assertEquals(projectVersion, modelVersion);
    }
}
