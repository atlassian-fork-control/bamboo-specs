package com.atlassian.bamboo.specs.model.plan.dependencies;

import com.atlassian.bamboo.specs.api.builders.plan.PlanIdentifier;
import com.atlassian.bamboo.specs.api.builders.plan.dependencies.Dependencies;
import com.atlassian.bamboo.specs.api.builders.plan.dependencies.DependenciesConfiguration;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationException;
import com.atlassian.bamboo.specs.api.model.plan.dependencies.DependenciesProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.codegen.BambooSpecsGenerator;
import org.junit.Assert;
import org.junit.Test;

import static com.atlassian.bamboo.specs.api.builders.plan.dependencies.DependenciesConfiguration.DependencyBlockingStrategy.BLOCK_IF_PARENT_HAS_CHANGES;
import static org.hamcrest.CoreMatchers.containsString;

public class DependenciesPropertiesJavalizationTest {

    @Test
    public void shouldJavalize() throws CodeGenerationException {
        //given
        final PlanIdentifier planIdentifier = new PlanIdentifier("FOO", "BAR");
        final PlanIdentifier planIdentifier2 = new PlanIdentifier("FO2", "BA2");
        final Dependencies dependencies = new Dependencies()
                .configuration(new DependenciesConfiguration()
                        .blockingStrategy(BLOCK_IF_PARENT_HAS_CHANGES)
                        .enabledForBranches(true)
                        .requireAllStagesPassing(true))
                .childPlans(planIdentifier, planIdentifier2);
        final DependenciesProperties properties = EntityPropertiesBuilders.build(dependencies);

        //when
        final String code = new BambooSpecsGenerator(properties).emitCode();

        //then
        Assert.assertThat(code, containsString(".childPlans(new PlanIdentifier(\"FOO\", \"BAR\"),\n"));
        Assert.assertThat(code, containsString("new PlanIdentifier(\"FO2\", \"BA2\"))"));
    }

}