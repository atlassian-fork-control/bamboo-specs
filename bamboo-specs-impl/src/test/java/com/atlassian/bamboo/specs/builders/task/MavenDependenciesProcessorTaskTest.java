package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.api.builders.requirement.Requirement;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.model.task.MavenDependenciesProcessorTaskProperties;
import org.apache.commons.lang3.RandomStringUtils;
import org.hamcrest.Matchers;
import org.junit.Test;

import java.util.Collections;

import static org.junit.Assert.assertThat;

public class MavenDependenciesProcessorTaskTest {

    @Test
    public void shouldValidateMinimalConfiguration() {
        new MavenDependenciesProcessorTask().build();
    }

    @Test
    public void shouldBeEqualOnProperties() {
        final Requirement requirement = Requirement.equals(RandomStringUtils.randomAlphabetic(5), RandomStringUtils.randomAlphabetic(5));

        assertThat(
            new MavenDependenciesProcessorTask()
                    .description("description")
                    .enabled(false)
                    .overrideProjectFile("overrideProjectFile")
                    .workingSubdirectory("workingSubdirectory")
                    .alternatePathForTheGlobalSettingsFile("alternatePathForTheGlobalSettingsFile")
                    .alternatePathForTheUserSettingsFile("alternatePathForTheUserSettingsFile")
                    .pathToMavenLocalRepository("pathToMavenLocalRepository")
                    .requirements(requirement)
                    .build(),
                Matchers.equalTo(new MavenDependenciesProcessorTaskProperties(
                        "description", false,
                        "overrideProjectFile", "workingSubdirectory",
                        "alternatePathForTheGlobalSettingsFile",
                        "alternatePathForTheUserSettingsFile",
                        "pathToMavenLocalRepository",
                        Collections.singletonList(EntityPropertiesBuilders.build(requirement)),
                        Collections.emptyList())));
    }

}