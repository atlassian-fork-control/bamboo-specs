package com.atlassian.bamboo.specs.codegen.emitters.task;

import com.atlassian.bamboo.specs.TestResourceHelper;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationContext;
import com.atlassian.bamboo.specs.api.model.repository.VcsRepositoryIdentifierProperties;
import com.atlassian.bamboo.specs.model.task.VcsPushTaskProperties;
import org.junit.Test;

import java.util.Collections;

import static org.junit.Assert.assertEquals;

public class VcsPushTaskEmitterTest {
    @Test
    public void emitCodeWithDefaultRepository() throws Exception {
        final VcsPushTaskProperties taskProperties = new VcsPushTaskProperties(
                "", true, Collections.emptyList(), Collections.emptyList(), true, null, "sub/directory");
        final String expectedCode = TestResourceHelper.getResourceAsString(getClass(), "taskWithDefaultRepository.java");
        final String emittedCode = new VcsPushTaskEmitter().emitCode(new CodeGenerationContext(), taskProperties);
        assertEquals(expectedCode, emittedCode);
    }

    @Test
    public void emitCodeWithCustomRepository() throws Exception {
        final VcsPushTaskProperties taskProperties = new VcsPushTaskProperties(
                "", true, Collections.emptyList(), Collections.emptyList(), false,
                new VcsRepositoryIdentifierProperties("bamboo-plugin", null), "sub/directory");
        final String expectedCode = TestResourceHelper.getResourceAsString(getClass(), "taskWithCustomRepository.java");
        final String emittedCode = new VcsPushTaskEmitter().emitCode(new CodeGenerationContext(), taskProperties);
        assertEquals(expectedCode, emittedCode);
    }
}
