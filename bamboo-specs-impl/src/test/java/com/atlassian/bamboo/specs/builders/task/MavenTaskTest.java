package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.model.task.MavenTaskProperties;
import org.junit.Test;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class MavenTaskTest {
    @Test
    public void builderMaven3Works() {
        final String goal = "clean test";
        final String environment = "environment";
        final String pomFile = "pom_mine.xml";
        final MavenTask mvn3Builder = new MavenTask()
                .goal(goal)
                .executableLabel("Maven 3")
                .environmentVariables(environment)
                .projectFile(pomFile);
        final MavenTaskProperties mvn3Properties = mvn3Builder.build();
        assertThat(mvn3Properties.getGoal(), is(goal));
        assertThat(mvn3Properties.getEnvironmentVariables(), is(environment));
        assertThat(mvn3Properties.getProjectFile(), is(pomFile));
        assertThat(mvn3Properties.getVersion(), is(3));
        assertThat(mvn3Properties.getAtlassianPlugin().getCompleteModuleKey(), containsString("mvn3"));
    }

    @Test
    public void builderMaven2Works() {
        final MavenTask mvn2Builder = new MavenTask()
                .version2()
                .goal("clean install")
                .executableLabel("Maven 2");
        final MavenTaskProperties mvn2Properties = mvn2Builder.build();
        assertThat(mvn2Properties.getVersion(), is(2));
        assertThat(mvn2Properties.getAtlassianPlugin().getCompleteModuleKey(), containsString("mvn2"));
    }
}