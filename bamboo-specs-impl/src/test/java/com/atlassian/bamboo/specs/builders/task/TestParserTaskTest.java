package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.api.builders.requirement.Requirement;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.model.task.TestParserTaskProperties;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.Arrays;
import java.util.Collections;

import static org.hamcrest.Matchers.not;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

public class TestParserTaskTest {

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void testEqualsOnDirectory() throws Exception {
        final Requirement requirement = Requirement.equals(RandomStringUtils.randomAlphabetic(5), RandomStringUtils.randomAlphabetic(5));
        assertThat(new TestParserTask(TestParserTaskProperties.TestType.JUNIT)
                        .description("junit task 1")
                        .enabled(true)
                        .resultDirectories("directory1")
                        .requirements(requirement)
                        .build(),
                not(equalTo(new TestParserTaskProperties(
                        TestParserTaskProperties.TestType.JUNIT,
                        "junit task 1",
                        true,
                        Collections.singletonList("directory2"),
                        null,
                        Collections.singletonList(EntityPropertiesBuilders.build(requirement)),
                        Collections.emptyList())))
        );
    }

    @Test
    public void testCreateJUnitParserTask() throws Exception {
        final TestParserTask jUnitParserTask = TestParserTask.createJUnitParserTask().defaultResultDirectory();
        assertThat(jUnitParserTask.getTestType(), equalTo(TestParserTaskProperties.TestType.JUNIT));
        assertThat(jUnitParserTask.build().getTestType(), equalTo(TestParserTaskProperties.TestType.JUNIT));
        assertThat(jUnitParserTask.build().getAtlassianPlugin().getCompleteModuleKey(),
                equalTo("com.atlassian.bamboo.plugins.testresultparser:task.testresultparser.junit"));
    }

    @Test
    public void testCreateTestNGParserTask() throws Exception {
        final TestParserTask testNGParserTask = TestParserTask.createTestNGParserTask().defaultResultDirectory();
        assertThat(testNGParserTask.getTestType(), equalTo(TestParserTaskProperties.TestType.TESTNG));
        assertThat(testNGParserTask.build().getTestType(), equalTo(TestParserTaskProperties.TestType.TESTNG));
        assertThat(testNGParserTask.build().getAtlassianPlugin().getCompleteModuleKey(),
                equalTo("com.atlassian.bamboo.plugins.testresultparser:task.testresultparser.testng"));
    }

    @Test
    public void testCreateNUnitParserTask() throws Exception {
        final TestParserTask testNUnitParserTask = TestParserTask.createNUnitParserTask().defaultResultDirectory();
        assertThat(testNUnitParserTask.getTestType(), equalTo(TestParserTaskProperties.TestType.NUNIT));
        assertThat(testNUnitParserTask.build().getTestType(), equalTo(TestParserTaskProperties.TestType.NUNIT));
        assertThat(testNUnitParserTask.build().getAtlassianPlugin().getCompleteModuleKey(),
                equalTo("com.atlassian.bamboo.plugin.dotnet:nunit"));
    }

    @Test
    public void testCreateMochaParserTask() throws Exception {
        final TestParserTask testNUnitParserTask = TestParserTask.createMochaParserTask().defaultResultDirectory();
        assertThat(testNUnitParserTask.getTestType(), equalTo(TestParserTaskProperties.TestType.MOCHA));
        assertThat(testNUnitParserTask.build().getTestType(), equalTo(TestParserTaskProperties.TestType.MOCHA));
        assertThat(testNUnitParserTask.build().getAtlassianPlugin().getCompleteModuleKey(),
                equalTo("com.atlassian.bamboo.plugins.bamboo-nodejs-plugin:task.reporter.mocha"));
    }

    @Test
    public void testCreateMSTestParserTask() throws Exception {
        final TestParserTask testNUnitParserTask = TestParserTask.createMSTestParserTask().defaultResultDirectory();
        assertThat(testNUnitParserTask.getTestType(), equalTo(TestParserTaskProperties.TestType.MSTEST));
        assertThat(testNUnitParserTask.build().getTestType(), equalTo(TestParserTaskProperties.TestType.MSTEST));
        assertThat(testNUnitParserTask.build().getAtlassianPlugin().getCompleteModuleKey(),
                equalTo("com.atlassian.bamboo.plugin.dotnet:mstest"));
    }

    @Test
    public void testEqualsOnPickUpTestResultsCreatedOutsideOfThisBuild() throws Exception {
        final Requirement requirement = Requirement.equals(RandomStringUtils.randomAlphabetic(5), RandomStringUtils.randomAlphabetic(5));
        assertThat(new TestParserTask(TestParserTaskProperties.TestType.JUNIT)
                        .description("junit task 1")
                        .enabled(true)
                        .resultDirectories("directory1")
                        .pickUpTestResultsCreatedOutsideOfThisBuild(false)
                        .requirements(requirement)
                        .build(),
                not(equalTo(new TestParserTaskProperties(
                        TestParserTaskProperties.TestType.JUNIT,
                        "junit task 1",
                        true,
                        Collections.singletonList("directory1"),
                        true,
                        Collections.singletonList(EntityPropertiesBuilders.build(requirement)),
                        Collections.emptyList())))
        );
    }

    @Test
    public void testBuildWithDefaultResultDirectoryJUnit() throws Exception {
        final Requirement requirement = Requirement.equals(RandomStringUtils.randomAlphabetic(5), RandomStringUtils.randomAlphabetic(5));
        assertThat(new TestParserTask(TestParserTaskProperties.TestType.JUNIT)
                        .description("junit task 1")
                        .enabled(true)
                        .defaultResultDirectory()
                        .requirements(requirement)
                        .build(),
                equalTo(new TestParserTaskProperties(
                        TestParserTaskProperties.TestType.JUNIT,
                        "junit task 1",
                        true,
                        Collections.singletonList("**/test-reports/*.xml"),
                        null,
                        Collections.singletonList(EntityPropertiesBuilders.build(requirement)),
                        Collections.emptyList()))
        );
    }

    @Test
    public void testBuildWithDefaultResultDirectoryTestNG() throws Exception {
        final Requirement requirement = Requirement.equals(RandomStringUtils.randomAlphabetic(5), RandomStringUtils.randomAlphabetic(5));
        assertThat(new TestParserTask(TestParserTaskProperties.TestType.TESTNG)
                        .description("testng task 1")
                        .enabled(true)
                        .defaultResultDirectory()
                        .requirements(requirement)
                        .build(),
                equalTo(new TestParserTaskProperties(
                        TestParserTaskProperties.TestType.TESTNG,
                        "testng task 1",
                        true,
                        Collections.singletonList("**/testng-results.xml"),
                        null,
                        Collections.singletonList(EntityPropertiesBuilders.build(requirement)),
                        Collections.emptyList()))
        );
    }

    @Test
    public void testBuildWithDefaultResultDirectoryMocha() throws Exception {
        final Requirement requirement = Requirement.equals(RandomStringUtils.randomAlphabetic(5), RandomStringUtils.randomAlphabetic(5));
        assertThat(new TestParserTask(TestParserTaskProperties.TestType.MOCHA)
                        .description("testng task 1")
                        .enabled(true)
                        .defaultResultDirectory()
                        .requirements(requirement)
                        .build(),
                equalTo(new TestParserTaskProperties(
                        TestParserTaskProperties.TestType.MOCHA,
                        "testng task 1",
                        true,
                        Collections.singletonList("mocha.json"),
                        null,
                        Collections.singletonList(EntityPropertiesBuilders.build(requirement)),
                        Collections.emptyList()))
        );
    }

    @Test
    public void testBuildWithDefaultResultDirectoryAllTestTypesSupported() throws Exception {
        for (TestParserTaskProperties.TestType testType : TestParserTaskProperties.TestType.values()) {
            new TestParserTask(testType).defaultResultDirectory();
        }
    }

    @Test
    public void testAtlassianPluginAllTestTypesSupported() throws Exception {
        for (TestParserTaskProperties.TestType testType : TestParserTaskProperties.TestType.values()) {
            new TestParserTask(testType).defaultResultDirectory().build().getAtlassianPlugin();
        }
    }

    @Test
    public void testBuildResultDirectories() throws Exception {
        final Requirement requirement = Requirement.equals(RandomStringUtils.randomAlphabetic(5), RandomStringUtils.randomAlphabetic(5));
        assertThat(new TestParserTask(TestParserTaskProperties.TestType.JUNIT)
                        .description("junit task 1")
                        .enabled(true)
                        .resultDirectories("directory1", "directory2")
                        .requirements(requirement)
                        .build(),
                equalTo(new TestParserTaskProperties(
                        TestParserTaskProperties.TestType.JUNIT,
                        "junit task 1",
                        true,
                        Arrays.asList("directory1", "directory2"),
                        null,
                        Collections.singletonList(EntityPropertiesBuilders.build(requirement)),
                        Collections.emptyList()))
        );
    }

    @Test
    public void testBuildWithResultDirectories() throws Exception {
        final Requirement requirement = Requirement.equals(RandomStringUtils.randomAlphabetic(5), RandomStringUtils.randomAlphabetic(5));
        assertThat(new TestParserTask(TestParserTaskProperties.TestType.JUNIT)
                        .description("junit task 1")
                        .enabled(true)
                        .resultDirectories("directory1")
                        .resultDirectories("directory2", "directory3")
                        .requirements(requirement)
                        .build(),
                equalTo(new TestParserTaskProperties(
                        TestParserTaskProperties.TestType.JUNIT,
                        "junit task 1",
                        true,
                        Arrays.asList("directory1", "directory2", "directory3"),
                        null,
                        Collections.singletonList(EntityPropertiesBuilders.build(requirement)),
                        Collections.emptyList()))
        );
    }

    @Test
    public void testResultDirectoriesNotNull() throws Exception {
        exception.expect(PropertiesValidationException.class);
        exception.expectMessage("Argument resultDirectories can not be null.");
        new TestParserTask(TestParserTaskProperties.TestType.JUNIT).resultDirectories((String) null);
        new TestParserTask(TestParserTaskProperties.TestType.JUNIT).resultDirectories((String[]) null);
    }

    @Test
    public void testBuildWithPickUpTestResultsCreatedOutsideOfThisBuild() throws Exception {
        final Requirement requirement = Requirement.equals(RandomStringUtils.randomAlphabetic(5), RandomStringUtils.randomAlphabetic(5));
        assertThat(new TestParserTask(TestParserTaskProperties.TestType.JUNIT)
                        .description("junit task 1")
                        .enabled(true)
                        .resultDirectories("directory1")
                        .pickUpTestResultsCreatedOutsideOfThisBuild(true)
                        .requirements(requirement)
                        .build(),
                equalTo(new TestParserTaskProperties(
                        TestParserTaskProperties.TestType.JUNIT,
                        "junit task 1",
                        true,
                        Arrays.asList("directory1"),
                        true,
                        Collections.singletonList(EntityPropertiesBuilders.build(requirement)),
                        Collections.emptyList()))
        );
    }

    @Test
    public void testBuildFailsWhenResultDirectoryEmpty() throws Exception {
        exception.expect(PropertiesValidationException.class);
        exception.expectMessage("Result directory not set.");
        new TestParserTask(TestParserTaskProperties.TestType.JUNIT)
                .description("junit task 1")
                .enabled(true)
                .pickUpTestResultsCreatedOutsideOfThisBuild(true)
                .build();
    }

}
