package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.api.builders.requirement.Requirement;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.model.task.BowerTaskProperties;
import org.junit.Test;

import java.util.Collections;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class BowerTaskTest {
    @Test(expected = PropertiesValidationException.class)
    public void testNodeExecutableIsRequired() {
        new BowerTask()
                .bowerExecutable("node_modules/bower/bin/bower")
                .command("install")
                .build();
    }

    @Test(expected = PropertiesValidationException.class)
    public void testBowerExecutableIsRequired() {
        new BowerTask()
                .nodeExecutable("Node.js 4.2")
                .bowerExecutable("")
                .command("install")
                .build();
    }

    @Test(expected = PropertiesValidationException.class)
    public void testCommandExecutableIsRequired() {
        new BowerTask()
                .nodeExecutable("Node.js 4.2")
                .bowerExecutable("node_modules/bower/bin/bower")
                .command("")
                .build();
    }

    @Test
    public void testMinimalConfigurationWorks() {
        // should not throw exception
        new BowerTask()
                .nodeExecutable("Node.js 6")
                .build();
    }

    @Test
    public void testAllParamsAreSetWhenBuildingProperties() {
        final String description = "install Bower dependencies";
        final boolean taskEnabled = false;
        final String nodeExecutable = "Node.js 6.0";
        final String bowerExecutable = "node_modules/bower/bin/bower";
        final String command = "install";
        final String environmentVariables = "NODE_HOME=/tmp/home";
        final String workingSubdirectory = "plugin";

        final Requirement requirement = Requirement.equals("key", "value");
        final BowerTask bowerTask = new BowerTask()
                .description(description)
                .enabled(taskEnabled)
                .nodeExecutable(nodeExecutable)
                .bowerExecutable(bowerExecutable)
                .command(command)
                .environmentVariables(environmentVariables)
                .workingSubdirectory(workingSubdirectory)
                .requirements(requirement);

        final BowerTaskProperties expectedProperties = new BowerTaskProperties(
                description,
                taskEnabled,
                nodeExecutable,
                environmentVariables,
                workingSubdirectory,
                bowerExecutable,
                command,
                Collections.singletonList(EntityPropertiesBuilders.build(requirement)),
                Collections.emptyList());

        assertThat(bowerTask.build(), is(equalTo(expectedProperties)));
    }
}
