package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.api.builders.requirement.Requirement;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.model.task.NpmTaskProperties;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;

import java.util.Collections;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class NpmTaskTest {
    @Test(expected = PropertiesValidationException.class)
    public void testCommandIsRequired() {
        new NpmTask()
                .nodeExecutable("Node.js 4.2")
                .build();
    }

    @Test(expected = PropertiesValidationException.class)
    public void testNodeExecutableIsRequired() {
        new NpmTask()
                .command("install")
                .build();
    }

    @Test
    public void testMinimalConfigurationWorks() {
        // should not throw exception
        new NpmTask()
                .nodeExecutable("Node.js 6")
                .command("test")
                .build();
    }

    @Test
    public void testAllParamsAreSetWhenBuildingProperties() {
        final String description = "install Node dependencies";
        final boolean taskEnabled = false;
        final String nodeExecutable = "Node.js 6.0";
        final String command = "install";
        final boolean useIsolatedCache = true;
        final String environmentVariables = "NODE_HOME=/tmp/home";
        final String workingSubdirectory = "plugin";

        final Requirement requirement = Requirement.equals(RandomStringUtils.randomAlphabetic(5), RandomStringUtils.randomAlphabetic(5));
        final NpmTask task = new NpmTask()
                .description(description)
                .enabled(taskEnabled)
                .nodeExecutable(nodeExecutable)
                .command(command)
                .useIsolatedCache(useIsolatedCache)
                .environmentVariables(environmentVariables)
                .workingSubdirectory(workingSubdirectory)
                .requirements(requirement);

        final NpmTaskProperties expectedProperties = new NpmTaskProperties(
                description,
                taskEnabled,
                nodeExecutable,
                environmentVariables,
                workingSubdirectory,
                command,
                useIsolatedCache,
                Collections.singletonList(EntityPropertiesBuilders.build(requirement)),
                Collections.emptyList());

        assertThat(task.build(), is(equalTo(expectedProperties)));
    }
}