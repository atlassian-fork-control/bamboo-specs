package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.api.builders.EntityPropertiesBuilder;
import com.atlassian.bamboo.specs.api.builders.plan.PlanIdentifier;
import com.atlassian.bamboo.specs.api.codegen.annotations.CodeGenerator;
import com.atlassian.bamboo.specs.api.model.plan.PlanIdentifierProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.codegen.emitters.task.ArtifactItemEmitter;
import com.atlassian.bamboo.specs.model.task.ArtifactItemProperties;
import org.jetbrains.annotations.NotNull;

/**
 * Represents an artifact or group of artifacts, obtained using one of the following methods.
 *
 * <ul>
 *     <li>declared in the job (as local or shared artifact)</li>
 *     <li>subscribed by the job (from a job from a previous stage)</li>
 *     <li>downloaded using Artifact Downloader task (from another plan)</li>
 * </ul>
 *
 * Examples:
 *
 * <pre>
 * // all artifacts from a current plan
 * new ArtifactItem()
 *
 * // an artifact from a current plan
 * new ArtifactItem()
 *     .artifact("name")
 *
 * // an artifact from another plan
 * new ArtifactItem()
 *     .sourcePlan(new PlanIdentifier("PROJ", "PLAN"))
 *     .artifact("name")
 * </pre>
 */
@CodeGenerator(ArtifactItemEmitter.class)
public class ArtifactItem extends EntityPropertiesBuilder<ArtifactItemProperties> {

    private boolean allArtifacts = true;
    private String artifactName;
    private PlanIdentifierProperties sourcePlan;

    public ArtifactItem() {

    }

    /**
     * Specify from which plan a downloaded artifact shall be taken. Note that by using few Artifact Downloader tasks
     * you can download several artifacts having the same name. This is to distinguish them.
     * In case a source plan is not specified, the current plan will be used.
     *
     * @param planIdentifier a project-plan key, e.g. "MYPROJECT-MYPLAN"
     * @return ArtifactItem this
     */
    public ArtifactItem sourcePlan(@NotNull final PlanIdentifier planIdentifier) {
        this.sourcePlan = EntityPropertiesBuilders.build(planIdentifier);
        return this;
    }

    /**
     * Specify that only one downloaded artifact shall be taken.
     *
     * @param artifactName name of the downloaded artifact
     * @return ArtifactItem this
     */
    public ArtifactItem artifact(@NotNull final String artifactName) {
        this.artifactName = artifactName;
        allArtifacts = false;
        return this;
    }

    /**
     * Specify that all downloaded artifacts shall be taken.
     *
     * @return ArtifactItem this
     */
    public ArtifactItem allArtifacts() {
        this.artifactName = null;
        allArtifacts = true;
        return this;
    }

    @Override
    public String toString() {
        return "ArtifactItem{" +
                "sourcePlan=" + sourcePlan +
                ", allArtifacts=" + allArtifacts +
                ", artifactName='" + artifactName + "'}";
    }

    @Override
    protected ArtifactItemProperties build() {
        return new ArtifactItemProperties(sourcePlan, allArtifacts, artifactName);
    }
}
