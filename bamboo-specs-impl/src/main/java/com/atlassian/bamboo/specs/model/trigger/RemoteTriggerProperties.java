package com.atlassian.bamboo.specs.model.trigger;

import com.atlassian.bamboo.specs.api.builders.AtlassianModule;
import com.atlassian.bamboo.specs.api.builders.trigger.RepositoryBasedTrigger.TriggeringRepositoriesType;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.repository.VcsRepositoryIdentifierProperties;
import com.atlassian.bamboo.specs.api.model.trigger.RepositoryBasedTriggerProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

import javax.annotation.concurrent.Immutable;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import static com.atlassian.bamboo.specs.validators.common.InternetAddressValidator.checkComaSeparatedIpAddressesOrCidrs;

@Immutable
public final class RemoteTriggerProperties extends RepositoryBasedTriggerProperties {
    @SuppressWarnings("WeakerAccess") //used in Bamboo tests
    public static final String NAME = "Remote trigger";
    public static final String MODULE_KEY = "com.atlassian.bamboo.triggers.atlassian-bamboo-triggers:remote";
    private static final AtlassianModuleProperties ATLASSIAN_MODULE = EntityPropertiesBuilders.build(new AtlassianModule(MODULE_KEY));

    private final String triggerIPAddresses;

    @NotNull
    @Override
    public AtlassianModuleProperties getAtlassianPlugin() {
        return ATLASSIAN_MODULE;
    }

    private RemoteTriggerProperties() {
        super(NAME, null, true, TriggeringRepositoriesType.ALL, Collections.emptyList());
        this.triggerIPAddresses = null;
    }

    public RemoteTriggerProperties(final String description,
                                   final boolean isEnabled,
                                   final TriggeringRepositoriesType triggeringRepositoriesType,
                                   final List<VcsRepositoryIdentifierProperties> selectedTriggeringRepositories,
                                   final String triggerIPAddresses) {
        super(NAME, description, isEnabled, triggeringRepositoriesType, selectedTriggeringRepositories);
        this.triggerIPAddresses = triggerIPAddresses;
        validate();
    }

    public String getTriggerIPAddresses() {
        return triggerIPAddresses;
    }

    public void validate() throws PropertiesValidationException {
        super.validate();
        if (StringUtils.isNotEmpty(triggerIPAddresses)) {
            checkComaSeparatedIpAddressesOrCidrs(triggerIPAddresses);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        final RemoteTriggerProperties that = (RemoteTriggerProperties) o;
        return Objects.equals(getTriggerIPAddresses(), that.getTriggerIPAddresses());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getTriggerIPAddresses());
    }
}
