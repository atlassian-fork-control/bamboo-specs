package com.atlassian.bamboo.specs.codegen.emitters.task;

import com.atlassian.bamboo.specs.api.builders.credentials.SharedCredentialsIdentifier;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationContext;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationException;
import com.atlassian.bamboo.specs.api.model.credentials.SharedCredentialsIdentifierProperties;
import com.atlassian.bamboo.specs.builders.task.DockerPullImageTask;
import com.atlassian.bamboo.specs.builders.task.DockerPushImageTask;
import com.atlassian.bamboo.specs.codegen.emitters.value.EntityPropertiesEmitter;
import com.atlassian.bamboo.specs.model.task.docker.DockerRegistryTaskProperties;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

public class DockerRegistryEmitter extends EntityPropertiesEmitter<DockerRegistryTaskProperties> {

    @NotNull
    @Override
    public String emitCode(@NotNull final CodeGenerationContext context, @NotNull final DockerRegistryTaskProperties entity) throws CodeGenerationException {
        switch (entity.getOperationType()) {
            case PUSH:
                builderClass = DockerPushImageTask.class;
                break;
            case PULL:
                builderClass = DockerPullImageTask.class;
                break;
            default:
                throw new IllegalStateException("Invalid operation type: " + entity.getOperationType());
        }

        fieldsToSkip.add("operationType");
        fieldsToSkip.add("registryType");
        fieldsToSkip.add("image");
        fieldsToSkip.add("username");
        fieldsToSkip.add("password");
        fieldsToSkip.add("email");

        String constructorInvocation = emitConstructorInvocation(context, entity);

        context.incIndentation();

        try {
            return String.format("%s%s.%s%s.%s",
                    constructorInvocation,
                    context.newLine(),
                    emitImageCode(entity),
                    context.newLine(),
                    emitAuthenticationCode(entity, context));
        } finally {
            context.decIndentation();
        }
    }

    protected String emitAuthenticationCode(DockerRegistryTaskProperties entity, CodeGenerationContext context) {
        final SharedCredentialsIdentifierProperties sharedCredentials = entity.getSharedCredentialsIdentifier();
        if (sharedCredentials != null) {
            return String.format("authentication(new %s(\"%s\"))", context.importClassName(SharedCredentialsIdentifier.class), sharedCredentials.getName());
        }
        if (StringUtils.isNotBlank(entity.getUsername())
                && StringUtils.isNotBlank(entity.getPassword())) {
            String escapedUsername = escapeString(entity.getUsername());
            String escapedPassword = escapeString(entity.getPassword());
            if (StringUtils.isNotBlank(entity.getEmail())) {
                String escapedEmail = escapeString(entity.getEmail());
                return String.format("authentication(\"%s\",\"%s\",\"%s\")",
                        escapedUsername,
                        escapedPassword,
                        escapedEmail);
            } else {
                return String.format("authentication(\"%s\",\"%s\")",
                        escapedUsername,
                        escapedPassword);
            }
        }
        return "defaultAuthentication()";
    }


    private String escapeString(String property) {
        return property.replaceAll("\"", "\\\\\"");
    }

    protected String emitImageCode(DockerRegistryTaskProperties entity) {
        switch (entity.getRegistryType()) {
            case DOCKER_HUB:
                return String.format("dockerHubImage(\"%s\")", entity.getImage());
            case CUSTOM:
                return String.format("customRegistryImage(\"%s\")", entity.getImage());
            default:
                throw new IllegalStateException("Invalid registry type: " + entity.getOperationType());
        }
    }
}
