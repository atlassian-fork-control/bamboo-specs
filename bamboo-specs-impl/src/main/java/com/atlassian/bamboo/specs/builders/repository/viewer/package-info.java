/**
 * Repository viewers, such as: Fisheye, Bitbucket Server and Cloud or 'hg serve' service.
 */
package com.atlassian.bamboo.specs.builders.repository.viewer;
