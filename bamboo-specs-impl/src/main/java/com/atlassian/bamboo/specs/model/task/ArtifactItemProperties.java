package com.atlassian.bamboo.specs.model.task;

import com.atlassian.bamboo.specs.api.model.EntityProperties;
import com.atlassian.bamboo.specs.api.model.plan.PlanIdentifierProperties;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkThat;

public class ArtifactItemProperties implements EntityProperties {

    @Nullable
    private final PlanIdentifierProperties sourcePlan;

    @Nullable
    private final String artifactName;

    private final boolean allArtifacts;

    private ArtifactItemProperties() {
        sourcePlan = null;
        artifactName = null;
        allArtifacts = false;
    }

    public ArtifactItemProperties(@Nullable PlanIdentifierProperties sourcePlan,
                                  boolean allArtifacts,
                                  @Nullable String artifactName) {
        this.sourcePlan = sourcePlan;
        this.artifactName = artifactName;
        this.allArtifacts = allArtifacts;
        validate();
    }

    @Nullable
    public PlanIdentifierProperties getSourcePlan() {
        return sourcePlan;
    }

    @Nullable
    public String getArtifactName() {
        return artifactName;
    }

    public boolean isAllArtifacts() {
        return allArtifacts;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ArtifactItemProperties that = (ArtifactItemProperties) o;
        return allArtifacts == that.allArtifacts &&
                Objects.equals(sourcePlan, that.sourcePlan) &&
                Objects.equals(artifactName, that.artifactName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sourcePlan, artifactName, allArtifacts);
    }

    @Override
    public void validate() {
        final ValidationContext context = ValidationContext.of("SCP task").with("Artifact");
        checkThat(context.with("Artifact name"), allArtifacts || artifactName != null,
                "Artifact name is required unless all artifacts are selected. You have name=%s and all artifacts=%b",
                artifactName, allArtifacts);
    }
}
