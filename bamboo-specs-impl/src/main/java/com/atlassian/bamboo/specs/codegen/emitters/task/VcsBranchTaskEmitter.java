package com.atlassian.bamboo.specs.codegen.emitters.task;

import com.atlassian.bamboo.specs.api.codegen.CodeGenerationContext;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationException;
import com.atlassian.bamboo.specs.builders.task.VcsBranchTask;
import com.atlassian.bamboo.specs.model.task.VcsBranchTaskProperties;
import org.jetbrains.annotations.NotNull;

public class VcsBranchTaskEmitter extends BaseVcsTaskEmitter<VcsBranchTaskProperties> {
    @NotNull
    @Override
    public String emitCode(@NotNull CodeGenerationContext context, @NotNull VcsBranchTaskProperties entity) throws CodeGenerationException {
        builderClass = VcsBranchTask.class;
        return super.emitCode(context, entity);
    }
}
