package com.atlassian.bamboo.specs.builders.task;

import com.atlassian.bamboo.specs.api.builders.task.Task;
import com.atlassian.bamboo.specs.model.task.docker.AbstractDockerTaskProperties;
import org.jetbrains.annotations.Nullable;


/**
 * Generic Docker task.
 */
public abstract class AbstractDockerTask<T extends AbstractDockerTask<T, P>, P extends AbstractDockerTaskProperties> extends Task<T, P> {
    @Nullable
    protected String environmentVariables;
    @Nullable
    protected String workingSubdirectory;

    /**
     * Environment variables which will be passed to Docker process.
     */
    public T environmentVariables(String environmentVariables) {
        this.environmentVariables = environmentVariables;
        return (T) this;
    }

    /**
     * An alternative subdirectory as working directory for this task.
     */
    public T workingSubdirectory(String workingSubdirectory) {
        this.workingSubdirectory = workingSubdirectory;
        return (T) this;
    }
}
