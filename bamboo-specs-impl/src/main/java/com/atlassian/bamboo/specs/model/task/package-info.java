/**
 * Tasks you can execute in a job.
 */
package com.atlassian.bamboo.specs.model.task;
