package com.atlassian.bamboo.specs.exceptions;

import org.jetbrains.annotations.Nullable;

/**
 * Exception thrown when an unexpected REST response was returned from Bamboo server.
 */
public class BambooSpecsRestRequestException extends RuntimeException {
    private final int statusCode;
    private final String responseEntity;

    /**
     * Creates instance of the exception.
     *
     * @param statusCode     HTTP status code returned by server
     * @param errorMessage   error message extracted from the response entity
     * @param responseEntity response entity returned by server
     */
    public BambooSpecsRestRequestException(int statusCode, @Nullable String errorMessage, @Nullable String responseEntity) {
        super(errorMessage);
        this.statusCode = statusCode;
        this.responseEntity = responseEntity;
    }

    /**
     * HTTP status code returned by server.
     */
    public int getStatusCode() {
        return statusCode;
    }

    /**
     * Error message extracted from {@link #getResponseEntity() HTTP response entity}.
     */
    @Override
    @Nullable
    public String getMessage() {
        return super.getMessage();
    }

    /**
     * HTTP response entity returned by server.
     */
    @Nullable
    public String getResponseEntity() {
        return responseEntity;
    }
}
