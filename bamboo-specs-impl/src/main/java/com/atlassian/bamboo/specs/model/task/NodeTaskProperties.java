package com.atlassian.bamboo.specs.model.task;

import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.plan.condition.ConditionProperties;
import com.atlassian.bamboo.specs.api.model.plan.requirement.RequirementProperties;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.annotation.concurrent.Immutable;
import java.util.List;
import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkThat;

@Immutable
public final class NodeTaskProperties extends BaseNodeTaskProperties {

    private static final AtlassianModuleProperties ATLASSIAN_PLUGIN =
            new AtlassianModuleProperties("com.atlassian.bamboo.plugins.bamboo-nodejs-plugin:task.builder.node");
    public static final ValidationContext VALIDATION_CONTEXT = ValidationContext.of("Node.js task");

    @NotNull
    private String script;
    @Nullable
    private String arguments;

    protected NodeTaskProperties() {
        super();
    }

    public NodeTaskProperties(@Nullable String description,
                              boolean enabled,
                              @NotNull String nodeExecutable,
                              @Nullable String environmentVariables,
                              @Nullable String workingSubdirectory,
                              @NotNull String script,
                              @Nullable String arguments,
                              @NotNull List<RequirementProperties> requirements,
                              @NotNull List<? extends ConditionProperties> conditions) throws PropertiesValidationException {
        super(description, enabled, nodeExecutable, environmentVariables, workingSubdirectory, requirements, conditions);
        this.script = script;
        this.arguments = arguments;

        validate();
    }

    @Override
    public void validate() {
        super.validate();
        checkThat(getValidationContext(), StringUtils.isNotBlank(script), "Script is not defined");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof NodeTaskProperties)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        final NodeTaskProperties that = (NodeTaskProperties) o;
        return Objects.equals(getScript(), that.getScript()) &&
                Objects.equals(getArguments(), that.getArguments());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getScript(), getArguments());
    }

    @NotNull
    @Override
    public AtlassianModuleProperties getAtlassianPlugin() {
        return ATLASSIAN_PLUGIN;
    }

    @Override
    protected ValidationContext getValidationContext() {
        return VALIDATION_CONTEXT;
    }

    @NotNull
    public String getScript() {
        return script;
    }

    @Nullable
    public String getArguments() {
        return arguments;
    }
}
