package com.atlassian.bamboo.specs.model.task;

import com.atlassian.bamboo.specs.api.codegen.annotations.CodeGenerator;
import com.atlassian.bamboo.specs.api.model.EntityProperties;
import com.atlassian.bamboo.specs.api.model.repository.VcsRepositoryIdentifierProperties;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import com.atlassian.bamboo.specs.codegen.emitters.task.CheckoutSpecEmitter;

import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkRequired;

@CodeGenerator(CheckoutSpecEmitter.class)
public class CheckoutItemProperties implements EntityProperties {
    private final VcsRepositoryIdentifierProperties repository;
    private final boolean defaultRepository;
    private final String path;

    private CheckoutItemProperties() {
        repository = null;
        defaultRepository = false;
        path = "";
    }

    public CheckoutItemProperties(final VcsRepositoryIdentifierProperties repository, final String path, boolean defaultRepository) {
        this.repository = repository;
        this.defaultRepository = defaultRepository;
        this.path = path;
        validate();
    }

    public static CheckoutItemProperties forDefaultRepository() {
        return new CheckoutItemProperties(null, "", true);
    }

    public boolean isDefaultRepository() {
        return defaultRepository;
    }

    public VcsRepositoryIdentifierProperties getRepository() {
        return repository;
    }

    public String getPath() {
        return path;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final CheckoutItemProperties that = (CheckoutItemProperties) o;
        return isDefaultRepository() == that.isDefaultRepository() &&
                Objects.equals(getRepository(), that.getRepository()) &&
                Objects.equals(getPath(), that.getPath());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getRepository(), isDefaultRepository(), getPath());
    }

    @Override
    public void validate() {
        final ValidationContext context = ValidationContext.of("VCS checkout task").with("Checkout spec");
        if (!defaultRepository) {
            checkRequired(context.with("repository"), repository);
        }
        checkRequired(context.with("path"), path);
    }
}
