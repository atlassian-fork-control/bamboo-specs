/**
 * Common validators, such as: IP addresses.
 */
package com.atlassian.bamboo.specs.validators.common;
