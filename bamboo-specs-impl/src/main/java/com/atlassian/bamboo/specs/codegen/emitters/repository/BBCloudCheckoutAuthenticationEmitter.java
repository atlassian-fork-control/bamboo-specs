package com.atlassian.bamboo.specs.codegen.emitters.repository;

public class BBCloudCheckoutAuthenticationEmitter extends AuthenticationEmitter {
    public BBCloudCheckoutAuthenticationEmitter() {
        super("checkoutAuthentication");
    }
}
