package com.atlassian.bamboo.specs.codegen.emitters.task;

import com.atlassian.bamboo.specs.api.codegen.CodeGenerationContext;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationException;
import com.atlassian.bamboo.specs.builders.task.VcsCommitTask;
import com.atlassian.bamboo.specs.model.task.VcsCommitTaskProperties;
import org.jetbrains.annotations.NotNull;

public class VcsCommitTaskEmitter extends BaseVcsTaskEmitter<VcsCommitTaskProperties> {
    @NotNull
    @Override
    public String emitCode(@NotNull CodeGenerationContext context, @NotNull VcsCommitTaskProperties entity) throws CodeGenerationException {
        builderClass = VcsCommitTask.class;
        return super.emitCode(context, entity);
    }
}
