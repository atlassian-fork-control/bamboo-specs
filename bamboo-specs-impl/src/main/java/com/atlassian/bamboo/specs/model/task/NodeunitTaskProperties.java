package com.atlassian.bamboo.specs.model.task;

import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.plan.condition.ConditionProperties;
import com.atlassian.bamboo.specs.api.model.plan.requirement.RequirementProperties;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import com.atlassian.bamboo.specs.builders.task.NodeunitTask;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.annotation.concurrent.Immutable;
import java.util.List;
import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkThat;

@Immutable
public final class NodeunitTaskProperties extends BaseNodeTaskProperties {
    private static final AtlassianModuleProperties ATLASSIAN_PLUGIN =
            new AtlassianModuleProperties("com.atlassian.bamboo.plugins.bamboo-nodejs-plugin:task.builder.nodeunit");
    public static final ValidationContext VALIDATION_CONTEXT = ValidationContext.of("Nodeunit task");

    @NotNull
    private String nodeunitExecutable = NodeunitTask.DEFAULT_NODEUNIT_EXECUTABLE;
    @NotNull
    private String testFilesAndDirectories = NodeunitTask.DEFAULT_TEST_DIRECTORY;
    @NotNull
    private String testResultsDirectory = NodeunitTask.DEFAULT_RESULTS_DIRECTORY;
    private boolean parseTestResults = NodeunitTask.DEFAULT_PARSE_TEST_RESULTS;
    @Nullable
    private String arguments;

    protected NodeunitTaskProperties() {
        super();
    }

    public NodeunitTaskProperties(@Nullable String description,
                                  boolean enabled,
                                  @NotNull String nodeExecutable,
                                  @Nullable String environmentVariables,
                                  @Nullable String workingSubdirectory,
                                  @NotNull String nodeunitExecutable,
                                  @NotNull String testFilesAndDirectories,
                                  @NotNull String testResultsDirectory,
                                  boolean parseTestResults,
                                  @Nullable String arguments,
                                  @NotNull List<RequirementProperties> requirements,
                                  @NotNull List<? extends ConditionProperties> conditions) throws PropertiesValidationException {
        super(description, enabled, nodeExecutable, environmentVariables, workingSubdirectory, requirements, conditions);
        this.nodeunitExecutable = nodeunitExecutable;
        this.testFilesAndDirectories = testFilesAndDirectories;
        this.testResultsDirectory = testResultsDirectory;
        this.parseTestResults = parseTestResults;
        this.arguments = arguments;

        validate();
    }

    @Override
    public void validate() {
        super.validate();
        checkThat(getValidationContext(), StringUtils.isNotBlank(nodeunitExecutable),
                "Nodeunit executable is not defined");
        checkThat(getValidationContext(), StringUtils.isNotBlank(testFilesAndDirectories),
                "Files and/or directories containing Nodeunit tests are not defined");
        checkThat(getValidationContext(), StringUtils.isNotBlank(testResultsDirectory),
                "Test results directory is not defined");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof NodeunitTaskProperties)) return false;
        if (!super.equals(o)) return false;
        NodeunitTaskProperties that = (NodeunitTaskProperties) o;
        return isParseTestResults() == that.isParseTestResults() &&
                Objects.equals(getNodeunitExecutable(), that.getNodeunitExecutable()) &&
                Objects.equals(getTestFilesAndDirectories(), that.getTestFilesAndDirectories()) &&
                Objects.equals(getTestResultsDirectory(), that.getTestResultsDirectory()) &&
                Objects.equals(getArguments(), that.getArguments());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getNodeunitExecutable(), getTestFilesAndDirectories(), getTestResultsDirectory(), isParseTestResults(), getArguments());
    }

    @NotNull
    @Override
    public AtlassianModuleProperties getAtlassianPlugin() {
        return ATLASSIAN_PLUGIN;
    }

    @Override
    protected ValidationContext getValidationContext() {
        return VALIDATION_CONTEXT;
    }

    @NotNull
    public String getNodeunitExecutable() {
        return nodeunitExecutable;
    }

    @NotNull
    public String getTestFilesAndDirectories() {
        return testFilesAndDirectories;
    }

    @NotNull
    public String getTestResultsDirectory() {
        return testResultsDirectory;
    }

    public boolean isParseTestResults() {
        return parseTestResults;
    }

    @Nullable
    public String getArguments() {
        return arguments;
    }
}
