package com.atlassian.bamboo.specs.builders.notification;

import com.atlassian.bamboo.specs.api.builders.notification.NotificationRecipient;
import com.atlassian.bamboo.specs.api.validators.common.ImporterUtils;
import com.atlassian.bamboo.specs.model.notification.GroupRecipientProperties;
import org.jetbrains.annotations.NotNull;


/**
 * Represents a group Bamboo can send notifications to.
 */
public class GroupRecipient extends NotificationRecipient<GroupRecipient, GroupRecipientProperties> {

    private String groupName;

    public GroupRecipient(@NotNull String groupName) {
        ImporterUtils.checkNotBlank("groupName", groupName);
        this.groupName = groupName;
    }

    @NotNull
    @Override
    protected GroupRecipientProperties build() {
        return new GroupRecipientProperties(groupName);
    }
}
