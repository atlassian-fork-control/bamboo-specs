package com.atlassian.bamboo.specs.codegen.emitters;


import com.atlassian.bamboo.specs.api.codegen.CodeEmitter;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationContext;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationException;
import com.atlassian.bamboo.specs.api.codegen.annotations.CodeGenerator;
import com.atlassian.bamboo.specs.api.codegen.annotations.CodeGeneratorName;
import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.AnnotatedElement;
import java.util.List;
import java.util.Map;
import java.util.Set;

public final class CodeGenerationUtils {
    private CodeGenerationUtils() {

    }

    /**
     * Generates comma separated list of items.
     */
    public static void appendCommaSeparatedList(CodeGenerationContext context,
                                                StringBuilder builder,
                                                List<String> valuesAndFails,
                                                Set<Integer> indicesOfFails) {
        for (int i = 0; i < valuesAndFails.size(); i++) {
            String valueOrFail = valuesAndFails.get(i);
            boolean isFail = indicesOfFails.contains(i);
            if (isFail) {
                builder.append("//").append(valueOrFail).append(context.newLine());
            } else {
                if (StringUtils.isNotEmpty(valueOrFail)) {
                    builder.append(valueOrFail);
                    if (hasMoreValuesToAppend(i, valuesAndFails, indicesOfFails)) {
                        builder.append(",");
                        if (i < valuesAndFails.size() - 1) {
                            builder.append(context.newLine());
                        }
                    }
                }
            }
        }
    }

    private static boolean hasMoreValuesToAppend(final int i, final List<String> valuesAndFails, Set<Integer> indicesOfFails) {
        for (int j = i + 1; j < valuesAndFails.size(); j++) {
            if (StringUtils.isNotEmpty(valuesAndFails.get(j)) && !indicesOfFails.contains(j)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks if value is empty. Object is empty if it is null, is a blank string or an empty collection.
     */
    public static boolean isEmptyField(final Object fieldValue) {
        if (fieldValue == null) {
            return true;
        } else if (fieldValue instanceof String) {
            return StringUtils.isBlank((String) fieldValue);
        } else if (fieldValue instanceof Iterable) {
            return !((Iterable) fieldValue).iterator().hasNext();
        } else if (fieldValue instanceof Map) {
            return ((Map) fieldValue).isEmpty();
        }
        return false;
    }

    /**
     * Checks if a field or class has a code generator specified by an annotation.
     *
     * @return instance of code generator or null if no annotation is present.
     */
    public static <T> CodeEmitter<T> findEmitterByAnnotation(final AnnotatedElement annotatedElement) throws CodeGenerationException {
        if (annotatedElement.isAnnotationPresent(CodeGenerator.class)) {
            final Class<? extends CodeEmitter<? extends Object>> emitterClass = ((CodeGenerator) annotatedElement.getAnnotation(CodeGenerator.class)).value();
            try {
                return (CodeEmitter<T>) emitterClass.newInstance();
            } catch (InstantiationException | IllegalAccessException e) {
                throw new CodeGenerationException("Could not instantiate emitter class " + emitterClass.getCanonicalName(), e);
            }
        }
        if (annotatedElement.isAnnotationPresent(CodeGeneratorName.class)) {
            final String emitterClassName = annotatedElement.getAnnotation(CodeGeneratorName.class).value();
            try {
                final Class<?> emitterClass = Class.forName(emitterClassName);
                return (CodeEmitter<T>) emitterClass.newInstance();
            } catch (final Exception e) {
                throw new CodeGenerationException("Could not instantiate emitter class " + emitterClassName, e);
            }
        }
        return null;
    }

}
