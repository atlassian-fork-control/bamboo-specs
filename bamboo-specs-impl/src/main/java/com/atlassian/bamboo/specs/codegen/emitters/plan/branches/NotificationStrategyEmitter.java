package com.atlassian.bamboo.specs.codegen.emitters.plan.branches;

import com.atlassian.bamboo.specs.api.codegen.CodeEmitter;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationContext;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationException;
import com.atlassian.bamboo.specs.api.model.plan.branches.PlanBranchManagementProperties;
import org.jetbrains.annotations.NotNull;

public class NotificationStrategyEmitter implements CodeEmitter<PlanBranchManagementProperties.NotificationStrategy> {
    @NotNull
    @Override
    public String emitCode(@NotNull CodeGenerationContext context, @NotNull PlanBranchManagementProperties.NotificationStrategy value) throws CodeGenerationException {
        switch (value) {

            case NOTIFY_COMMITTERS:
                return ".notificationForCommitters()";
            case INHERIT:
                return ".notificationLikeParentPlan()";
            case NONE:
                return ".notificationDisabled()";
            default:
                throw new CodeGenerationException("Unrecognized notification strategy: " + value);
        }
    }
}




