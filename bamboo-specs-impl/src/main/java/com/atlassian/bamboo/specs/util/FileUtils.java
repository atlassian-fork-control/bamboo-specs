package com.atlassian.bamboo.specs.util;

import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

public final class FileUtils {

    private FileUtils() {
    }

    /**
     * Read file content.
     * @param path to file
     * @param fileIsNullErrorMessage error message thrown when file is null
     * @param fileReadErrorMessage error message thrown when can't read file
     * @return file body
     */
    public static String readFileContent(@NotNull final Path path, final String fileIsNullErrorMessage, final String fileReadErrorMessage) {
        checkNotNull(fileIsNullErrorMessage, path);
        if (Files.notExists(path)) {
            throw new PropertiesValidationException(String.format("File %s does not exist", path.toAbsolutePath()));
        }
        try {
            return readFileToString(path);
        } catch (final IOException e) {
            throw new PropertiesValidationException(String.format(fileReadErrorMessage, path.toAbsolutePath()), e);
        }
    }

    public static String readFileToString(final Path path) throws IOException {
        return new String(Files.readAllBytes(path), StandardCharsets.UTF_8);
    }
}
