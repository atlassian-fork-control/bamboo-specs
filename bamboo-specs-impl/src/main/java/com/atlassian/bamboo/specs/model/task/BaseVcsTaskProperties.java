package com.atlassian.bamboo.specs.model.task;

import com.atlassian.bamboo.specs.api.model.plan.condition.ConditionProperties;
import com.atlassian.bamboo.specs.api.model.plan.requirement.RequirementProperties;
import com.atlassian.bamboo.specs.api.model.repository.VcsRepositoryIdentifierProperties;
import com.atlassian.bamboo.specs.api.model.task.TaskProperties;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkRequired;

public abstract class BaseVcsTaskProperties extends TaskProperties {
    private final boolean defaultRepository;
    @Nullable
    private final VcsRepositoryIdentifierProperties repository;
    @Nullable
    private final String workingSubdirectory;

    protected BaseVcsTaskProperties() {
        super();
        this.defaultRepository = false;
        this.repository = null;
        this.workingSubdirectory = null;
    }

    protected BaseVcsTaskProperties(@Nullable String description,
                                    boolean enabled,
                                    @NotNull List<RequirementProperties> requirements,
                                    @NotNull List<? extends ConditionProperties> conditions,
                                    boolean defaultRepository,
                                    @Nullable VcsRepositoryIdentifierProperties repository,
                                    @Nullable String workingSubdirectory) {
        super(description, enabled, requirements, conditions);
        this.defaultRepository = defaultRepository;
        this.repository = repository;
        this.workingSubdirectory = workingSubdirectory;
    }

    @NotNull
    protected abstract ValidationContext getValidationContext();

    @Override
    public void validate() {
        super.validate();
        if (!defaultRepository) {
            checkRequired(getValidationContext().with("repository"), repository);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BaseVcsTaskProperties)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        final BaseVcsTaskProperties that = (BaseVcsTaskProperties) o;
        return isDefaultRepository() == that.isDefaultRepository() &&
                Objects.equals(getRepository(), that.getRepository()) &&
                Objects.equals(getWorkingSubdirectory(), that.getWorkingSubdirectory());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), isDefaultRepository(), getRepository(), getWorkingSubdirectory());
    }

    public boolean isDefaultRepository() {
        return defaultRepository;
    }

    @Nullable
    public VcsRepositoryIdentifierProperties getRepository() {
        return repository;
    }

    @Nullable
    public String getWorkingSubdirectory() {
        return workingSubdirectory;
    }
}
