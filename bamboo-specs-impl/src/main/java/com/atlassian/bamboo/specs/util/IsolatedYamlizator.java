package com.atlassian.bamboo.specs.util;


import com.atlassian.bamboo.specs.api.model.EntityProperties;

import java.util.function.Function;

public final class IsolatedYamlizator {
    private static final Function<EntityProperties, String> DUMP_PROPERTIES = entityProperties -> Yamlizator.getYaml().dump(entityProperties);

    private static final IsolatedExecutor<EntityProperties, String> YAMLIZATOR = new IsolatedExecutor<>(DUMP_PROPERTIES, "Yamlizator thread");

    static {
        YAMLIZATOR.start();
    }

    private IsolatedYamlizator() {
    }

    public static String execute(final BambooSpecProperties bambooSpecProperties) {
        try {
            return YAMLIZATOR.execute(bambooSpecProperties);
        } catch (final Throwable e) {
            throw new RuntimeException(e);
        }
    }

    public static Thread getThread() {
        Yamlizator.getYaml(); //run all static initialisation upfront
        return YAMLIZATOR.getThread();
    }
}
