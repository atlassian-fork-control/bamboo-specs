package com.atlassian.bamboo.specs.codegen.emitters.fragment;

import com.atlassian.bamboo.specs.api.codegen.CodeEmitter;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationContext;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationException;
import org.jetbrains.annotations.NotNull;

public class NoArgCallEmitter<T extends Object> implements CodeEmitter<T> {

    private final String methodName;

    public NoArgCallEmitter(@NotNull String methodName) {
        this.methodName = methodName;
    }

    @NotNull
    @Override
    public String emitCode(@NotNull CodeGenerationContext context, @NotNull T value) throws CodeGenerationException {
        return "." + methodName + "()";
    }
}
