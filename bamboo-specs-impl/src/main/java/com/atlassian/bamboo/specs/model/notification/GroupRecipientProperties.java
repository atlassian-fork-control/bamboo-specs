package com.atlassian.bamboo.specs.model.notification;


import com.atlassian.bamboo.specs.api.codegen.annotations.ConstructFrom;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.notification.NotificationRecipientProperties;
import com.atlassian.bamboo.specs.api.validators.common.ImporterUtils;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;

@ConstructFrom({"groupName"})
public class GroupRecipientProperties extends NotificationRecipientProperties {
    private static final AtlassianModuleProperties ATLASSIAN_PLUGIN = new AtlassianModuleProperties("com.atlassian.bamboo.plugin.system.notifications:recipient.group");

    private final String groupName;

    private GroupRecipientProperties() {
        groupName = null;
    }

    public GroupRecipientProperties(String groupName) {
        this.groupName = groupName;
        validate();
    }

    public String getGroupName() {
        return groupName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GroupRecipientProperties that = (GroupRecipientProperties) o;
        return Objects.equals(getGroupName(), that.getGroupName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getGroupName());
    }

    @NotNull
    @Override
    public AtlassianModuleProperties getAtlassianPlugin() {
        return ATLASSIAN_PLUGIN;
    }


    @Override
    public void validate() {
        ValidationContext validationContext = ValidationContext.of("groupRecipient");
        ImporterUtils.checkNotBlank(validationContext, "groupName", groupName);
    }
}
