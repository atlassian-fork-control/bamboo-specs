/**
 * Various implementations of {@link com.atlassian.bamboo.specs.api.codegen.CodeEmitter} which are used for converting
 * {@link com.atlassian.bamboo.specs.api.builders.plan.branches.PlanBranchManagement}-related settings model into Java.
 */
package com.atlassian.bamboo.specs.codegen.emitters.plan.branches;
