package com.atlassian.bamboo.specs.codegen.emitters.notification;

import com.atlassian.bamboo.specs.api.codegen.CodeEmitter;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationContext;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationException;
import org.jetbrains.annotations.NotNull;

public class BuildErrorNotificationEmitter implements CodeEmitter<Boolean> {

    @NotNull
    @Override
    public String emitCode(@NotNull final CodeGenerationContext context, @NotNull final Boolean value) throws CodeGenerationException {
        if (value) {
            return ".sendForFirstOccurrenceOnly()";
        } else {
            return ".sendForAllErrors()";
        }
    }
}
