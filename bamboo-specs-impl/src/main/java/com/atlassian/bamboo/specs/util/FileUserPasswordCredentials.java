package com.atlassian.bamboo.specs.util;

import com.atlassian.bamboo.specs.util.Logger.LogLevel;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Provides Bamboo credentials from text file.
 * <p>
 * It is advised not to store the file in a code repository for security reasons.
 * <p>
 * Format of the text file:
 * <pre>{@code
 *     username = some_user_name
 *     password = some_user_password
 * }</pre>
 */
public class FileUserPasswordCredentials implements UserPasswordCredentials {
    private static final Logger log = Logger.getLogger(FileUserPasswordCredentials.class);

    private static final String USERNAME_PROPERTY = "username";
    private static final String PASSWORD_PROPERTY = "password";

    private final String credentialsLocation;
    private final String username;
    private final String password;

    public FileUserPasswordCredentials() {
        this(".credentials");
    }

    public FileUserPasswordCredentials(@NotNull String credentialsLocation) {
        this.credentialsLocation = credentialsLocation;
        final Properties properties = new Properties();
        try {
            final File credentialsFile = new File(credentialsLocation);
            log.debug("Looking for credentials file: %s", credentialsFile.getAbsolutePath());

            if (credentialsFile.exists()) {
                log.debug("Credentials file found: %s", credentialsFile.getAbsolutePath());
                properties.load(new FileReader(credentialsFile));
            } else {
                log.debug("Credentials file not found under '%s', scanning JVM classpath for '%s'", credentialsFile.getAbsolutePath(), credentialsLocation);
                final InputStream resourceAsStream = getClass().getClassLoader().getResourceAsStream(credentialsLocation);
                if (resourceAsStream == null) {
                    log.debug("Credentials not found on JVM classpath: %s", credentialsLocation);
                    log.info("Couldn't find credentials file '%s' to access Bamboo", credentialsLocation);
                    log.info("Please make sure that the path points to an existing file or to a resource available on JVM classpath");
                    LogUtils.hintLogLevel(log, LogLevel.DEBUG);
                    throw new RuntimeException(String.format("Couldn't find credentials file: %s", credentialsLocation));
                }

                log.debug("Found credentials file on JVM classpath: %s", credentialsLocation);
                properties.load(RestHelper.class.getClassLoader().getResourceAsStream(credentialsLocation));
            }
        } catch (final IOException e) {
            log.info("Couldn't read credentials file '%s' to access Bamboo", credentialsLocation);
            log.info("Please make sure that the file is accessible and that it is in correct format (Java properties)");
            LogUtils.hintLogLevel(log, LogLevel.DEBUG);
            throw new RuntimeException(e);
        }

        this.username = getPropertyOrThrow(properties, USERNAME_PROPERTY, credentialsLocation);
        this.password = getPropertyOrThrow(properties, PASSWORD_PROPERTY, credentialsLocation);
    }

    private String getPropertyOrThrow(Properties properties, String propertyKey, String fileName) {
        final String propertyValue = properties.getProperty(propertyKey);
        if (StringUtils.isEmpty(propertyValue)) {
            log.info("Property '%s' must be defined in credentials file '%s' to access Bamboo", propertyKey, fileName);
            log.info("Please make sure that the file is in correct format (Java properties)");
            throw new RuntimeException(String.format("Property '%s' was not defined in credentials file: %s", propertyKey, fileName));
        } else {
            return propertyValue;
        }
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public String toString() {
        return String.format("[type: 'file', location: '%s', username: '%s']", credentialsLocation, username);
    }
}
