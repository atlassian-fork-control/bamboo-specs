package com.atlassian.bamboo.specs.model.task;

import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.plan.condition.ConditionProperties;
import com.atlassian.bamboo.specs.api.model.plan.requirement.RequirementProperties;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import com.atlassian.bamboo.specs.builders.task.GulpTask;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.annotation.concurrent.Immutable;
import java.util.List;
import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkThat;

@Immutable
public final class GulpTaskProperties extends BaseNodeTaskProperties {
    private static final AtlassianModuleProperties ATLASSIAN_PLUGIN =
            new AtlassianModuleProperties("com.atlassian.bamboo.plugins.bamboo-nodejs-plugin:task.builder.gulp");
    public static final ValidationContext VALIDATION_CONTEXT = ValidationContext.of("Gulp task");

    @NotNull
    private String gulpExecutable = GulpTask.DEFAULT_GULP_EXECUTABLE;
    @Nullable
    private String task;
    @Nullable
    private String gulpfile;

    protected GulpTaskProperties() {
        super();
    }

    public GulpTaskProperties(@Nullable String description,
                              boolean enabled,
                              @NotNull String nodeExecutable,
                              @Nullable String environmentVariables,
                              @Nullable String workingSubdirectory,
                              @NotNull String gulpExecutable,
                              @Nullable String task,
                              @Nullable String gulpfile,
                              @NotNull List<RequirementProperties> requirements,
                              @NotNull List<? extends ConditionProperties> conditions) throws PropertiesValidationException {
        super(description, enabled, nodeExecutable, environmentVariables, workingSubdirectory, requirements, conditions);
        this.gulpExecutable = gulpExecutable;
        this.task = task;
        this.gulpfile = gulpfile;

        validate();
    }

    @Override
    public void validate() {
        super.validate();
        checkThat(getValidationContext(), StringUtils.isNotBlank(gulpExecutable), "Gulp executable is not defined");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof GulpTaskProperties)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        final GulpTaskProperties that = (GulpTaskProperties) o;
        return Objects.equals(gulpExecutable, that.gulpExecutable) &&
                Objects.equals(task, that.task) &&
                Objects.equals(gulpfile, that.gulpfile);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), gulpExecutable, task, gulpfile);
    }

    @NotNull
    @Override
    public AtlassianModuleProperties getAtlassianPlugin() {
        return ATLASSIAN_PLUGIN;
    }

    @Override
    protected ValidationContext getValidationContext() {
        return VALIDATION_CONTEXT;
    }

    @NotNull
    public String getGulpExecutable() {
        return gulpExecutable;
    }

    @Nullable
    public String getTask() {
        return task;
    }

    @Nullable
    public String getGulpfile() {
        return gulpfile;
    }
}
