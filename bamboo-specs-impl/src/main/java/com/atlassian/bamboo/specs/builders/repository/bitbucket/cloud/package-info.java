/**
 * Bitbucket Cloud repository type.
 */
package com.atlassian.bamboo.specs.builders.repository.bitbucket.cloud;
