package com.atlassian.bamboo.specs.util;

import org.yaml.snakeyaml.constructor.Constructor;
import org.yaml.snakeyaml.error.YAMLException;
import org.yaml.snakeyaml.nodes.Tag;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

public class WhitelistedYamlConstructor extends Constructor {
    /**
     * A set of regular expressions.
     */
    private static final Set<Pattern> WHITELISTED_CLASSES = Collections.unmodifiableSet(new HashSet<Pattern>() {{
        // our top-level entity
        add(Pattern.compile("com\\.atlassian\\.bamboo\\.specs\\.util\\.BambooSpecProperties"));
        // properties from API
        add(Pattern.compile("com\\.atlassian\\.bamboo\\.specs\\.api\\.model\\..*Properties"));
        add(Pattern.compile("com\\.atlassian\\.bamboo\\.specs\\.api\\.model\\..*Properties\\$.+"));
        // properties from IMPL
        add(Pattern.compile("com\\.atlassian\\.bamboo\\.specs\\.model\\..*Properties"));
        add(Pattern.compile("com\\.atlassian\\.bamboo\\.specs\\.model\\..*Properties\\$.+"));
        // enums from the builders package
        add(Pattern.compile("com\\.atlassian\\.bamboo\\.specs\\.api\\.builders\\.Applicability"));
        add(Pattern.compile("com\\.atlassian\\.bamboo\\.specs\\.api\\.builders\\.permission\\.PermissionType"));
    }});


    WhitelistedYamlConstructor() {
        for (final CustomYamlers.CustomYamler customYamler : CustomYamlers.YAMLERS) {
            this.yamlConstructors.put(new Tag(customYamler.getYamledClass()), customYamler.getConstructor());
        }
    }

    boolean isClassAllowed(String fullClassName) {
        return WHITELISTED_CLASSES.stream()
                .anyMatch(pattern -> pattern.matcher(fullClassName).matches());
    }

    @Override
    protected Class<?> getClassForName(String name) throws ClassNotFoundException {
        if (isClassAllowed(name)) {
            return super.getClassForName(name);
        } else {
            throw new YAMLException(String.format("Class '%s' is not allowed in YAML.", name));
        }
    }

}
