package com.atlassian.bamboo.specs.builders.repository.viewer;

import com.atlassian.bamboo.specs.api.builders.repository.viewer.VcsRepositoryViewer;
import com.atlassian.bamboo.specs.api.validators.common.ImporterUtils;
import com.atlassian.bamboo.specs.model.repository.viewer.GenericRepositoryViewerProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Represents a generic viewer.
 * <p>
 * Generic viewer is simplistic implementation of commit link generator done on the 'best effort' basis.
 * More specialised implementations, e.g. {@link FishEyeRepositoryViewer} or {@link BitbucketCloudRepositoryViewer} should be preferred, if applicable.
 */
public class GenericRepositoryViewer extends VcsRepositoryViewer {
    private String viewerUrl;
    private String repositoryPath;

    /**
     * Specifies a generic viewer.
     */
    public GenericRepositoryViewer() {
    }

    /**
     * Sets url to be used for link generation.
     */
    public GenericRepositoryViewer viewerUrl(@NotNull String viewerUrl) {
        ImporterUtils.checkNotNull("viewerUrl", viewerUrl);
        this.viewerUrl = viewerUrl;
        return this;
    }

    /**
     * Sets a relative path in remote repository. By default, it is empty.
     * <p>
     * When checking out a module from a repository, for example SVN repository, a file path shown in Bamboo is relative to that module and
     * not the repository root. If that's the case, the missing part of the path needs to be defined here.
     */
    public GenericRepositoryViewer repositoryPath(@Nullable String repositoryPath) {
        this.repositoryPath = repositoryPath;
        return this;
    }

    @NotNull
    @Override
    protected GenericRepositoryViewerProperties build() {
        return new GenericRepositoryViewerProperties(viewerUrl, repositoryPath);
    }
}
