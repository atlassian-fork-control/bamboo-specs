package com.atlassian.bamboo.specs.util;

import com.atlassian.bamboo.specs.util.Logger.LogLevel;
import org.jetbrains.annotations.NotNull;

public final class LogUtils {
    private LogUtils() {
    }

    /**
     * Logs a hint to enable more detailed logging to obtain additional information. If the logger is at sufficient
     * details level, this method does nothing.
     *
     * @param log            an instance of the logger
     * @param suggestedLevel suggested level of logging for additional information
     */
    public static void hintLogLevel(@NotNull Logger log, @NotNull LogLevel suggestedLevel) {
        if (!Logger.LOG_LEVEL.shouldLog(suggestedLevel)) {
            log.info("(for more information, enable more detailed logging with system property -D%s=%s)", Logger.LOG_LEVEL_PROPERTY, suggestedLevel.name());
        }
    }
}
