package com.atlassian.bamboo.specs.model.task;

import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.plan.condition.ConditionProperties;
import com.atlassian.bamboo.specs.api.model.plan.requirement.RequirementProperties;
import com.atlassian.bamboo.specs.api.model.task.TaskProperties;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkThat;

public abstract class BaseNodeTaskProperties extends TaskProperties {
    @NotNull
    private String nodeExecutable;
    @Nullable
    private String environmentVariables;
    @Nullable
    private String workingSubdirectory;

    public BaseNodeTaskProperties() {
        super();
    }

    public BaseNodeTaskProperties(@Nullable String description,
                                  boolean enabled,
                                  @NotNull String nodeExecutable,
                                  @Nullable String environmentVariables,
                                  @Nullable String workingSubdirectory,
                                  @NotNull List<RequirementProperties> requirements,
                                  @NotNull List<? extends ConditionProperties> conditions) throws PropertiesValidationException {
        super(description, enabled, requirements, conditions);
        this.nodeExecutable = nodeExecutable;
        this.environmentVariables = environmentVariables;
        this.workingSubdirectory = workingSubdirectory;
    }

    protected abstract ValidationContext getValidationContext();

    @Override
    public void validate() {
        super.validate();
        checkThat(getValidationContext(), StringUtils.isNotBlank(nodeExecutable), "Node.js executable is not defined");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BaseNodeTaskProperties)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        final BaseNodeTaskProperties that = (BaseNodeTaskProperties) o;
        return Objects.equals(getNodeExecutable(), that.getNodeExecutable()) &&
                Objects.equals(getEnvironmentVariables(), that.getEnvironmentVariables()) &&
                Objects.equals(getWorkingSubdirectory(), that.getWorkingSubdirectory());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getNodeExecutable(), getEnvironmentVariables(), getWorkingSubdirectory());
    }

    @NotNull
    public String getNodeExecutable() {
        return nodeExecutable;
    }

    @Nullable
    public String getEnvironmentVariables() {
        return environmentVariables;
    }

    @Nullable
    public String getWorkingSubdirectory() {
        return workingSubdirectory;
    }
}
