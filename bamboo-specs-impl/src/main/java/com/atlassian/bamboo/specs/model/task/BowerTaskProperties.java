package com.atlassian.bamboo.specs.model.task;

import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.plan.condition.ConditionProperties;
import com.atlassian.bamboo.specs.api.model.plan.requirement.RequirementProperties;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import com.atlassian.bamboo.specs.builders.task.BowerTask;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.annotation.concurrent.Immutable;
import java.util.List;
import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkThat;

@Immutable
public final class BowerTaskProperties extends BaseNodeTaskProperties {
    private static final AtlassianModuleProperties ATLASSIAN_PLUGIN =
            new AtlassianModuleProperties("com.atlassian.bamboo.plugins.bamboo-nodejs-plugin:task.builder.bower");
    public static final ValidationContext VALIDATION_CONTEXT = ValidationContext.of("Bower task");

    @NotNull
    private String bowerExecutable = BowerTask.DEFAULT_BOWER_EXECUTABLE;
    @NotNull
    private String command = BowerTask.DEFAULT_BOWER_COMMAND;

    protected BowerTaskProperties() {
        super();
    }

    public BowerTaskProperties(@Nullable String description,
                               boolean enabled,
                               @NotNull String nodeExecutable,
                               @Nullable String environmentVariables,
                               @Nullable String workingSubdirectory,
                               @NotNull String bowerExecutable,
                               @NotNull String command,
                               final List<RequirementProperties> requirements,
                               @NotNull List<? extends ConditionProperties> conditions) throws PropertiesValidationException {
        super(description, enabled, nodeExecutable, environmentVariables, workingSubdirectory, requirements, conditions);
        this.bowerExecutable = bowerExecutable;
        this.command = command;

        validate();
    }

    @Override
    public void validate() {
        super.validate();
        checkThat(getValidationContext(), StringUtils.isNotBlank(bowerExecutable), "Bower executable is not defined");
        checkThat(getValidationContext(), StringUtils.isNotBlank(command), "Command is not defined");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BowerTaskProperties)) return false;
        if (!super.equals(o)) return false;
        BowerTaskProperties that = (BowerTaskProperties) o;
        return Objects.equals(getBowerExecutable(), that.getBowerExecutable()) &&
                Objects.equals(getCommand(), that.getCommand());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getBowerExecutable(), getCommand());
    }

    @NotNull
    @Override
    public AtlassianModuleProperties getAtlassianPlugin() {
        return ATLASSIAN_PLUGIN;
    }

    @Override
    protected ValidationContext getValidationContext() {
        return VALIDATION_CONTEXT;
    }

    @NotNull
    public String getBowerExecutable() {
        return bowerExecutable;
    }

    @NotNull
    public String getCommand() {
        return command;
    }
}
