package com.atlassian.bamboo.specs.builders.notification;


import com.atlassian.bamboo.specs.api.builders.notification.NotificationType;
import com.atlassian.bamboo.specs.model.notification.BuildErrorNotificationProperties;
import org.jetbrains.annotations.NotNull;

/**
 * Represents notifications send on build error.
 */
public class BuildErrorNotification extends NotificationType<BuildErrorNotification, BuildErrorNotificationProperties> {
    private boolean firstOccurrenceOnly = false;

    public BuildErrorNotification sendForFirstOccurrenceOnly() {
        firstOccurrenceOnly = true;
        return this;
    }

    public BuildErrorNotification sendForAllErrors() {
        firstOccurrenceOnly = false;
        return this;
    }

    @NotNull
    @Override
    protected BuildErrorNotificationProperties build() {
        return new BuildErrorNotificationProperties(firstOccurrenceOnly);
    }
}
