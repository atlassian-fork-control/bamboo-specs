package com.atlassian.bamboo.specs.codegen.emitters.plan.branches;

import com.atlassian.bamboo.specs.api.builders.plan.branches.BranchCleanup;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationContext;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationException;
import com.atlassian.bamboo.specs.api.model.plan.branches.BranchCleanupProperties;
import com.atlassian.bamboo.specs.codegen.emitters.value.EntityPropertiesEmitter;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

public class BranchCleanupEmitter extends EntityPropertiesEmitter<BranchCleanupProperties> {
    @NotNull
    @Override
    public String emitCode(@NotNull CodeGenerationContext context, @NotNull BranchCleanupProperties entity) throws CodeGenerationException {
        builderClass = BranchCleanup.class;

        fieldsToSkip.add("removeDeletedFromRepository");
        fieldsToSkip.add("removeInactiveInRepository");
        fieldsToSkip.add("removeDeletedFromRepositoryPeriod");
        fieldsToSkip.add("removeInactiveInRepositoryPeriod");

        StringBuilder builder = new StringBuilder();
        builder.append(emitConstructorInvocation(context, entity));

        if (entity.isRemoveDeletedFromRepository()) {
            builder.append(context.newLine());
            final long removedAfterDays = entity.getRemoveDeletedFromRepositoryPeriod().toDays();
            builder.append(".whenRemovedFromRepositoryAfterDays(")
                    .append(removedAfterDays)
                    .append(")");
        }

        if (entity.isRemoveInactiveInRepository()) {
            builder.append(context.newLine());
            final long inactiveAfterDays = entity.getRemoveInactiveInRepositoryPeriod().toDays();
            builder.append(".whenInactiveInRepositoryAfterDays(")
                    .append(inactiveAfterDays)
                    .append(")");
        }

        String otherFields = emitFields(context, entity);
        if (StringUtils.isNotBlank(otherFields)) {
            builder.append(context.newLine());
            builder.append(otherFields);
        }

        return builder.toString();
    }
}




