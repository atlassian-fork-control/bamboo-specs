package com.atlassian.bamboo.specs.codegen.emitters.repository;

import com.atlassian.bamboo.specs.api.codegen.CodeGenerationContext;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationException;
import com.atlassian.bamboo.specs.codegen.emitters.fragment.FieldSetterEmitter;
import org.jetbrains.annotations.NotNull;

public class BBCloudRepositorySlugEmitter extends FieldSetterEmitter<String> {
    public BBCloudRepositorySlugEmitter() {
        super("repositorySlug");
    }

    @NotNull
    @Override
    public String emitCode(@NotNull final CodeGenerationContext context, @NotNull final String value) throws CodeGenerationException {
        final int index = value.indexOf('/');
        final String owner = value.substring(0, index);
        final String repository = value.substring(index + 1);
        return String.format(".repositorySlug(\"%s\", \"%s\")", owner, repository);
    }
}
