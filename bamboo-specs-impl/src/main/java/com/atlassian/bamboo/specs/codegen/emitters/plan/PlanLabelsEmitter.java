package com.atlassian.bamboo.specs.codegen.emitters.plan;

import com.atlassian.bamboo.specs.api.codegen.CodeEmitter;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationContext;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationException;
import com.atlassian.bamboo.specs.api.model.label.EmptyLabelsListProperties;
import com.atlassian.bamboo.specs.api.model.label.LabelProperties;
import com.atlassian.bamboo.specs.codegen.emitters.CodeGenerationUtils;
import com.atlassian.bamboo.specs.codegen.emitters.value.ValueEmitterFactory;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class PlanLabelsEmitter implements CodeEmitter<Iterable<LabelProperties>> {
    @NotNull
    @Override
    public String emitCode(@NotNull CodeGenerationContext context, @NotNull Iterable<LabelProperties> value) throws CodeGenerationException {
        final List<LabelProperties> labelProperties = StreamSupport.stream(value.spliterator(), false).collect(Collectors.toList());

        if (labelProperties.size() == 1 && labelProperties.get(0) instanceof EmptyLabelsListProperties) {
            return ".noLabels()";
        } else {
            final StringBuilder result = new StringBuilder(".labels(");
            final List<String> valuesAndFails = new ArrayList<>();
            final Set<Integer> failed = new HashSet<>();

            int count = 0;
            for (LabelProperties labelProperty : labelProperties) {
                final String label = labelProperty.getName();
                try {
                    final CodeEmitter<String> emitter = ValueEmitterFactory.emitterFor(label);
                    final String code = emitter.emitCode(context, label);
                    valuesAndFails.add(code);
                } catch (CodeGenerationException e) {
                    valuesAndFails.add(e.getMessage());
                    failed.add(count);
                }
                count++;
            }

            context.incIndentation();
            CodeGenerationUtils.appendCommaSeparatedList(context, result, valuesAndFails, failed);
            context.decIndentation();

            return result.append(")").toString();
        }
    }
}
