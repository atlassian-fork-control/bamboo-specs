/**
 * Various exceptions thrown by Bamboo Specs. Exceptions from this package are intended to be used internally.
 */
package com.atlassian.bamboo.specs.exceptions;
