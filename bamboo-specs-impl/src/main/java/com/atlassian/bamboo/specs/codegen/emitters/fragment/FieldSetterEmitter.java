package com.atlassian.bamboo.specs.codegen.emitters.fragment;

import com.atlassian.bamboo.specs.api.codegen.CodeEmitter;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationContext;
import com.atlassian.bamboo.specs.api.codegen.CodeGenerationException;
import com.atlassian.bamboo.specs.codegen.emitters.value.ValueEmitterFactory;
import org.jetbrains.annotations.NotNull;

/**
 * Default code generator for any field of {@link com.atlassian.bamboo.specs.api.model.EntityProperties} instance.
 * Implementation should emit full method invocation statement, starting with ".methodName".
 */
public class FieldSetterEmitter<T extends Object> implements CodeEmitter<T> {
    protected final String methodName;

    public FieldSetterEmitter(final String methodName) {
        this.methodName = methodName;
    }

    /**
     * Generates invocation of a setter method with a single argument.
     */
    @NotNull
    public String emitCode(@NotNull final CodeGenerationContext context, @NotNull final T argument) throws CodeGenerationException {
        StringBuilder builder = new StringBuilder(".").append(methodName).append("(");
        try {
            final CodeEmitter<T> codeEmitter = ValueEmitterFactory.emitterFor(argument);
            context.incIndentation();
            String argumentCode = codeEmitter.emitCode(context, argument);
            context.decIndentation();
            builder.append(argumentCode).append(")");
        } catch (CodeGenerationException e) {
            context.decIndentation();
            throw new CodeGenerationException("Cannot generate invocation of method " + methodName + ": " + e.getMessage());
        }
        return builder.toString();
    }
}
