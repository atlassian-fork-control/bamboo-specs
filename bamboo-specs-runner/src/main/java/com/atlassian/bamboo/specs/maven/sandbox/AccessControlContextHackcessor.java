package com.atlassian.bamboo.specs.maven.sandbox;

import java.lang.reflect.Method;
import java.security.AccessControlContext;
import java.security.AccessController;
import java.security.ProtectionDomain;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashSet;

/**
 * This class exists to provide access to {@link AccessControlContext#getContext()} method, which is package protected.
 */
public final class AccessControlContextHackcessor {
    private AccessControlContextHackcessor() {
    }

    private static ProtectionDomain[] getFullContext() {
        final AccessControlContext acc = AccessController.getContext();

        try {
            final Method getContextMethod = AccessControlContext.class.getDeclaredMethod("getContext");
            getContextMethod.setAccessible(true);
            return (ProtectionDomain[]) getContextMethod.invoke(acc);
        } catch (final Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Returns the list of protection domains of the code available on stack. It will exclude the protection domain of the supplied class.
     * If the returned list is empty, the stack contained only system/privileged code.
     */
    public static Collection<ProtectionDomain> getContext(final Class<?> pdToExclude) {
        final Collection<ProtectionDomain> protectionDomains = new LinkedHashSet<>(Arrays.asList(getFullContext()));
        protectionDomains.remove(pdToExclude.getProtectionDomain());
        return protectionDomains;
    }
}
