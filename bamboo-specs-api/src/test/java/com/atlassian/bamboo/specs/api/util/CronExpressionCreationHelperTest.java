package com.atlassian.bamboo.specs.api.util;

import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

@RunWith(JUnitParamsRunner.class)
public class CronExpressionCreationHelperTest {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Parameters(method = "scheduleEveryData")
    @Test
    public void testScheduleEvery(boolean shouldFail, int every, TimeUnit unit, String expectedExpression) throws Exception {
        if (shouldFail) {
            expectedException.expect(PropertiesValidationException.class);
        }

        String result = CronExpressionCreationHelper.scheduleEvery(every, unit);

        assertThat(result, equalTo(expectedExpression));
    }

    @SuppressWarnings("unused") //used by Parameters annotation
    private Object[] scheduleEveryData() {
        return new Object[]{
                new Object[]{true, 1, null, ""},
                new Object[]{true, 1, TimeUnit.MILLISECONDS, ""},
                new Object[]{true, 1, TimeUnit.MICROSECONDS, ""},
                new Object[]{true, 1, TimeUnit.NANOSECONDS, ""},
                new Object[]{true, 1, TimeUnit.DAYS, ""},
                new Object[]{true, 0, TimeUnit.SECONDS, ""},
                new Object[]{true, -1, TimeUnit.SECONDS, ""},
                new Object[]{true, -2, TimeUnit.MINUTES, ""},
                new Object[]{true, -3, TimeUnit.HOURS, ""},
                new Object[]{false, 30, TimeUnit.SECONDS, "0/30 * * ? * *"},
                new Object[]{false, 15, TimeUnit.MINUTES, "0 0/15 * ? * *"},
                new Object[]{false, 8, TimeUnit.HOURS, "0 0 0/8 ? * *"},
        };
    }

    @Parameters(method = "scheduleOnceDailyData")
    @Test
    public void testScheduleOnceDaily(boolean shouldFail, LocalTime at, String expectedExpression) throws Exception {
        if (shouldFail) {
            expectedException.expect(PropertiesValidationException.class);
        }

        String result = CronExpressionCreationHelper.scheduleOnceDaily(at);

        assertThat(result, equalTo(expectedExpression));
    }

    @SuppressWarnings("unused") //used by Parameters annotation
    private Object[] scheduleOnceDailyData() {
        return new Object[]{
                new Object[]{true, null, ""},
                new Object[]{false, LocalTime.of(15, 13), "0 13 15 ? * *"},
                new Object[]{false, LocalTime.of(8, 0), "0 0 8 ? * *"}
        };
    }

    @Parameters(method = "scheduleWeeklyData")
    @Test
    public void testScheduleWeekly(boolean shouldFail, LocalTime at, DayOfWeek dayOfMonth, String expectedExpression) throws Exception {
        if (shouldFail) {
            expectedException.expect(PropertiesValidationException.class);
        }

        String result = CronExpressionCreationHelper.scheduleWeekly(at, dayOfMonth);

        assertThat(result, equalTo(expectedExpression));
    }

    @SuppressWarnings("unused") //used by Parameters annotation
    private Object[] scheduleWeeklyData() {
        return new Object[]{
                new Object[]{true, null, DayOfWeek.FRIDAY, ""},
                new Object[]{false, LocalTime.of(14, 13), DayOfWeek.SUNDAY, "0 13 14 ? * 1"},
                new Object[]{false, LocalTime.of(14, 13), DayOfWeek.SATURDAY, "0 13 14 ? * 7"},
                new Object[]{false, LocalTime.of(14, 13), DayOfWeek.WEDNESDAY, "0 13 14 ? * 4"}
        };
    }

    @Test
    public void testScheduleWeeklyMoreDays() throws Exception {
        String result = CronExpressionCreationHelper
                .scheduleWeekly(LocalTime.of(8, 8), DayOfWeek.FRIDAY,
                        DayOfWeek.FRIDAY,
                        DayOfWeek.SATURDAY,
                        DayOfWeek.WEDNESDAY);

        assertThat(result, equalTo("0 8 8 ? * 6,6,7,4"));
    }

    @Parameters(method = "scheduleMonthlyData")
    @Test
    public void testScheduleMonthly(boolean shouldFail, LocalTime at, int dayOfMonth, String expectedExpression) throws Exception {
        if (shouldFail) {
            expectedException.expect(PropertiesValidationException.class);
        }

        String result = CronExpressionCreationHelper.scheduleMonthly(at, dayOfMonth);

        assertThat(result, equalTo(expectedExpression));
    }

    @SuppressWarnings("unused") //used by Parameters annotation
    private Object[] scheduleMonthlyData() {
        return new Object[]{
                new Object[]{true, null, 1, ""},
                new Object[]{false, LocalTime.of(13, 0), -1, "0 0 13 -1 * ?"}, //cron is validated properly on Bamboo side only
                new Object[]{false, LocalTime.of(8, 0), 43, "0 0 8 43 * ?"}, //cron is validated properly on Bamboo side only
                new Object[]{false, LocalTime.of(13, 0), 11, "0 0 13 11 * ?"},
                new Object[]{false, LocalTime.of(3, 0), 11, "0 0 3 11 * ?"},
        };
    }

}
