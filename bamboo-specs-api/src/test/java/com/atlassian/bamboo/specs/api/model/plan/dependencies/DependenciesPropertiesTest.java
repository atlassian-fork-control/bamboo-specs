package com.atlassian.bamboo.specs.api.model.plan.dependencies;

import com.atlassian.bamboo.specs.api.builders.BambooKey;
import com.atlassian.bamboo.specs.api.builders.plan.Job;
import com.atlassian.bamboo.specs.api.builders.plan.Plan;
import com.atlassian.bamboo.specs.api.builders.plan.PlanIdentifier;
import com.atlassian.bamboo.specs.api.builders.plan.Stage;
import com.atlassian.bamboo.specs.api.builders.plan.branches.BranchCleanup;
import com.atlassian.bamboo.specs.api.builders.plan.branches.PlanBranchManagement;
import com.atlassian.bamboo.specs.api.builders.plan.configuration.ConcurrentBuilds;
import com.atlassian.bamboo.specs.api.builders.plan.dependencies.Dependencies;
import com.atlassian.bamboo.specs.api.builders.plan.dependencies.DependenciesConfiguration;
import com.atlassian.bamboo.specs.api.builders.plan.dependencies.EmptyDependenciesList;
import com.atlassian.bamboo.specs.api.builders.project.Project;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.plan.PlanIdentifierProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import org.junit.Assert;
import org.junit.Test;

import static com.atlassian.bamboo.specs.api.builders.plan.dependencies.DependenciesConfiguration.DependencyBlockingStrategy.BLOCK_IF_PARENT_HAS_CHANGES;
import static org.hamcrest.CoreMatchers.equalTo;

public class DependenciesPropertiesTest {

    @Test
    public void shouldProduceValidProperties() {
        //given
        final PlanIdentifier planIdentifier = new PlanIdentifier("FOO", "BAR");

        //when
        final Dependencies dependencies = new Dependencies()
                .configuration(new DependenciesConfiguration()
                        .blockingStrategy(BLOCK_IF_PARENT_HAS_CHANGES)
                        .enabledForBranches(true)
                        .requireAllStagesPassing(true))
                .childPlans(planIdentifier);
        final DependenciesProperties properties = EntityPropertiesBuilders.build(dependencies);

        //then
        final PlanIdentifierProperties planIdentifierProperties = properties.getChildPlans().get(0);
        Assert.assertThat(planIdentifierProperties.getProjectKey().getKey(), equalTo("FOO"));
        Assert.assertThat(planIdentifierProperties.getKey().getKey(), equalTo("BAR"));

        final DependenciesConfigurationProperties configurationProperties = properties.getDependenciesConfigurationProperties();
        Assert.assertThat(configurationProperties.getBlockingStrategy(), equalTo(BLOCK_IF_PARENT_HAS_CHANGES));
        Assert.assertThat(configurationProperties.isEnabledForBranches(), equalTo(true));
        Assert.assertThat(configurationProperties.isRequireAllStagesPassing(), equalTo(true));
    }

    @Test(expected = PropertiesValidationException.class)
    public void shouldRejectInvalidProperties() {
        //given
        final PlanIdentifier planIdentifier = new PlanIdentifier("FOO", "BAR");

        //when
        final Dependencies dependencies = new Dependencies()
                .configuration(null)
                .childPlans(planIdentifier);
        final DependenciesProperties properties = EntityPropertiesBuilders.build(dependencies);
    }

    @Test
    public void shouldValidateEmptyDependenciesList() {
        //given
        Plan plan = new Plan(new Project()
                .key(new BambooKey("BAR")),
                "Bar",
                new BambooKey("FOO"))
                .description("foo bar")
                .pluginConfigurations(new ConcurrentBuilds())
                .stages(new Stage("Default Stage")
                        .jobs(new Job("Default Job",
                                new BambooKey("JOB1"))
                        ))
                .planBranchManagement(new PlanBranchManagement()
                        .delete(new BranchCleanup())
                        .notificationForCommitters())
                .dependencies(new EmptyDependenciesList());

        //when
        EntityPropertiesBuilders.build(plan);

        //then
        // plan is valid
    }

}