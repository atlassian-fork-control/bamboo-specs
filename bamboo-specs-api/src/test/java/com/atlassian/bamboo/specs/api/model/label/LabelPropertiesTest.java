package com.atlassian.bamboo.specs.api.model.label;

import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import org.junit.Test;

public class LabelPropertiesTest {
    @Test(expected = PropertiesValidationException.class)
    public void validationTriggeredByConstructor() {
        new LabelProperties("!invalid");
    }
}
