package com.atlassian.bamboo.specs.api.model.deployment;


import com.atlassian.bamboo.specs.api.builders.deployment.ReleaseNaming;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class ReleaseNamingPropertiesTest {
    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    @Test
    public void namingWithAutoIncrementNoNumber() throws PropertiesValidationException {
        expectedException.expect(PropertiesValidationException.class);
        EntityPropertiesBuilders.build(new ReleaseNaming("ABC").autoIncrement(true));
    }

    @Test
    public void namingWithoutAutoIncrementNoNumber() throws PropertiesValidationException {
        EntityPropertiesBuilders.build(new ReleaseNaming("ABC").autoIncrement(false));
    }

    @Test
    public void namingWithAutoIncrementWithNumber() throws PropertiesValidationException {
        EntityPropertiesBuilders.build(new ReleaseNaming("ABC2").autoIncrement(false));
    }

    @Test
    public void namingWithPasswordVariable() throws PropertiesValidationException {
        expectedException.expect(PropertiesValidationException.class);
        EntityPropertiesBuilders.build(new ReleaseNaming("ABC2_${bamboo.password}").autoIncrement(false));
    }

    @Test
    public void namingWithIncrementedPasswordVariable() throws PropertiesValidationException {
        expectedException.expect(PropertiesValidationException.class);
        EntityPropertiesBuilders.build(new ReleaseNaming("ABC2${bamboo.hello_password}")
                .autoIncrement(false)
                .variablesToAutoIncrement("hello_password"));
    }

    @Test
    public void namingWithVariableNotUsedInNaming() throws PropertiesValidationException {
        expectedException.expect(PropertiesValidationException.class);
        EntityPropertiesBuilders.build(new ReleaseNaming("ABC2")
                .autoIncrement(false)
                .variablesToAutoIncrement("hello"));
    }

    @Test
    public void namingWithVariableUsedInNaming() throws PropertiesValidationException {
        EntityPropertiesBuilders.build(new ReleaseNaming("ABC2${bamboo.hello}")
                .autoIncrement(false)
                .variablesToAutoIncrement("hello"));
    }
}
