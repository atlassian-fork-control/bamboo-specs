package com.atlassian.bamboo.specs.api.validators;

import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

@RunWith(JUnitParamsRunner.class)
public class CronExpressionClientSideValidatorTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Parameters(method = "validateData")
    @Test
    public void validate(boolean shouldFail, String cronExpression) throws Exception {
        if (shouldFail) {
            expectedException.expect(PropertiesValidationException.class);
        }

        CronExpressionClientSideValidator.validate(cronExpression);
    }

    @SuppressWarnings("unused") //used by Parameters annotation
    private Object[] validateData() {
        return new Object[]{
                new Object[]{true, ""},
                new Object[]{true, "totally not valid expression"},
                new Object[]{false, "a b c d e f"}, //cron is validated properly on Bamboo side only
                new Object[]{false, "0 0 13 -1 * ?"}, //cron is validated properly on Bamboo side only
                new Object[]{false, "0 0 8 43 * ?"}, //cron is validated properly on Bamboo side only
                new Object[]{false, "0 0 3 11 * ?"}
        };
    }

}
