package com.atlassian.bamboo.specs.api.model.plan.condition;

import com.atlassian.bamboo.specs.api.builders.condition.AnyTaskCondition;
import com.atlassian.bamboo.specs.api.codegen.annotations.Builder;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import org.jetbrains.annotations.NotNull;

import javax.annotation.concurrent.Immutable;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

@Immutable
@Builder(AnyTaskCondition.class)
public class AnyConditionProperties implements ConditionProperties {
    protected final AtlassianModuleProperties atlassianPlugin;
    protected final Map<String, String> configuration;

    private AnyConditionProperties() {
        this(null, new HashMap<>());
    }

    public AnyConditionProperties(@NotNull AtlassianModuleProperties atlassianPlugin,
                                  @NotNull Map<String, String> configuration) {
        this.atlassianPlugin = atlassianPlugin;
        this.configuration = configuration;
    }

    @NotNull
    public Map<String, String> getConfiguration() {
        return new HashMap<>(configuration);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AnyConditionProperties that = (AnyConditionProperties) o;
        return Objects.equals(atlassianPlugin, that.atlassianPlugin) &&
                Objects.equals(configuration, that.configuration);
    }

    @Override
    public int hashCode() {
        return Objects.hash(atlassianPlugin, configuration);
    }

    @Override
    public AtlassianModuleProperties getAtlassianPlugin() {
        return atlassianPlugin;
    }

    @Override
    public void validate() {
        checkNotNull("atlassianPlugin", atlassianPlugin);
    }
}
