/**
 * Items stored in plan configuration, including (but not limited to) items from 'Miscellaneous' page.
 */
package com.atlassian.bamboo.specs.api.builders.plan.configuration;
