/**
 * Automatic branch creation, integration and cleanup.
 */
package com.atlassian.bamboo.specs.api.builders.plan.branches;
