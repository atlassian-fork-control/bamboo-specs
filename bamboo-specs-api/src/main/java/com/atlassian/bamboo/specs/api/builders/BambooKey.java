package com.atlassian.bamboo.specs.api.builders;

import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.BambooKeyProperties;
import org.jetbrains.annotations.NotNull;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

/**
 * Represents a short key of an Bamboo entity (project, plan, job, etc).
 * This is partial key, for instance the value of plan key is "PLAN" rather than "PROJECT-PLAN".
 * Keys serve as secondary identifiers of Bamboo entities, that is, whenever both key and {@link BambooOid}
 * is present, {@link BambooOid} has higher priority.
 *
 * @see BambooOid
 */
public class BambooKey extends EntityPropertiesBuilder<BambooKeyProperties> {
    private String key;

    /**
     * Specify key of provided value.
     */
    public BambooKey(@NotNull String key) throws PropertiesValidationException {
        checkNotNull("key", key);

        this.key = key;
    }

    protected BambooKeyProperties build() throws PropertiesValidationException {
        return new BambooKeyProperties(key);
    }

    @Override
    public String toString() {
        return key;
    }
}
