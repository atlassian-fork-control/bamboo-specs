package com.atlassian.bamboo.specs.api.exceptions;

import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import com.atlassian.bamboo.specs.api.validators.common.ValidationProblem;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Errors in export properties objects (mostly required properties, and context free validation).
 */
public class PropertiesValidationException extends RuntimeException {
    private final List<ValidationProblem> errors;

    public PropertiesValidationException(@NotNull final String message) {
        this(message, null);
    }

    public PropertiesValidationException(@NotNull final String message, @Nullable Throwable cause) {
        this(Collections.singletonList(new ValidationProblem(message)), cause);
    }

    public PropertiesValidationException(@NotNull ValidationContext validationContext, @NotNull final String message) {
        this(new ValidationProblem(validationContext, message));
    }

    public PropertiesValidationException(@NotNull final List<ValidationProblem> errors) {
        this(errors, null);
    }

    public PropertiesValidationException(@NotNull final ValidationProblem error) {
        this(Collections.singletonList(error), null);
    }

    public PropertiesValidationException(@NotNull final List<ValidationProblem> errors, @Nullable Throwable cause) {
        super(errorsToMessage(errors), cause);
        this.errors = errors;
    }

    public List<ValidationProblem> getErrors() {
        return errors;
    }

    private static String errorsToMessage(@NotNull final List<ValidationProblem> errors) {
        return errors.stream()
                .map(ValidationProblem::getMessage)
                .collect(Collectors.joining("\n------------------------------\n"));
    }
}
