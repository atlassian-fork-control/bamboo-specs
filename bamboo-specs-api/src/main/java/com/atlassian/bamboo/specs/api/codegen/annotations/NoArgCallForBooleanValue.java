package com.atlassian.bamboo.specs.api.codegen.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Indicates method that should be called if the field is boolean and its value matches the value.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface NoArgCallForBooleanValue {
    String name() default "";

    boolean value() default true;
}
