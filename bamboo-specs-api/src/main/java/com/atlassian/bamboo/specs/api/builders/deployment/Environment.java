package com.atlassian.bamboo.specs.api.builders.deployment;

import com.atlassian.bamboo.specs.api.builders.Applicability;
import com.atlassian.bamboo.specs.api.builders.EntityPropertiesBuilder;
import com.atlassian.bamboo.specs.api.builders.Variable;
import com.atlassian.bamboo.specs.api.builders.deployment.configuration.EnvironmentPluginConfiguration;
import com.atlassian.bamboo.specs.api.builders.docker.DockerConfiguration;
import com.atlassian.bamboo.specs.api.builders.notification.Notification;
import com.atlassian.bamboo.specs.api.builders.requirement.Requirement;
import com.atlassian.bamboo.specs.api.builders.task.Task;
import com.atlassian.bamboo.specs.api.builders.trigger.Trigger;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.VariableProperties;
import com.atlassian.bamboo.specs.api.model.deployment.EnvironmentProperties;
import com.atlassian.bamboo.specs.api.model.deployment.configuration.EnvironmentPluginConfigurationProperties;
import com.atlassian.bamboo.specs.api.model.docker.DockerConfigurationProperties;
import com.atlassian.bamboo.specs.api.model.notification.NotificationProperties;
import com.atlassian.bamboo.specs.api.model.notification.NotificationRecipientProperties;
import com.atlassian.bamboo.specs.api.model.plan.requirement.RequirementProperties;
import com.atlassian.bamboo.specs.api.model.task.TaskProperties;
import com.atlassian.bamboo.specs.api.model.trigger.TriggerProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.api.validators.common.ImporterUtils;
import com.atlassian.bamboo.specs.api.validators.common.ValidationProblem;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

/**
 * Represents a deployment environment.
 */
public class Environment extends EntityPropertiesBuilder<EnvironmentProperties> {

    private String name;
    private String description;
    private List<TaskProperties> tasks = new ArrayList<>();
    private List<TaskProperties> finalTasks = new ArrayList<>();
    private List<TriggerProperties> triggers = new ArrayList<>();
    private List<VariableProperties> variables = new ArrayList<>();
    private List<RequirementProperties> requirements = new ArrayList<>();
    private List<NotificationProperties> notifications = new ArrayList<>();
    private DockerConfigurationProperties dockerConfiguration;
    private Map<String, EnvironmentPluginConfigurationProperties> pluginConfigurations = new LinkedHashMap<>();

    /**
     * Specifies environment.
     *
     * @param name name of the environment.
     */
    public Environment(@NotNull String name) {
        ImporterUtils.checkNotNull("name", name);
        this.name = name;
        this.dockerConfiguration(new DockerConfiguration().enabled(false));
    }

    /**
     * Sets the environment description.
     */
    public Environment description(String description) {
        this.description = description;
        return this;
    }

    /**
     * Adds provided {@link Task}s to the list of tasks executed by the environment.
     */
    public Environment tasks(@NotNull Task<?, ?>... tasks) {
        checkNotNull("tasks", tasks);
        for (Task<?, ?> t : tasks) {
            TaskProperties task = EntityPropertiesBuilders.build(t);
            if (!task.applicableTo().contains(Applicability.DEPLOYMENTS)) {
                throw new PropertiesValidationException("Task " + t.getClass().getSimpleName() + " is not available in deployments");
            } else {
                addTask(this.tasks, task);
            }
        }
        return this;
    }

    /**
     * Adds provided {@link Task}s to the list of the final tasks executed by the environment.
     * <p>Final tasks for a environment are always executed, even if previous tasks in the environment failed.
     */
    public Environment finalTasks(@NotNull Task<?, ?>... finalTasks) {
        checkNotNull("finalTasks", finalTasks);
        for (Task<?, ?> t : finalTasks) {
            TaskProperties task = EntityPropertiesBuilders.build(t);
            if (!task.applicableTo().contains(Applicability.DEPLOYMENTS)) {
                throw new PropertiesValidationException("Task " + t.getClass().getSimpleName() + " is not available in deployments");
            } else {
                addTask(this.finalTasks, task);
            }
        }
        return this;
    }

    private void addTask(final List<TaskProperties> tasks, final TaskProperties task) {
        requirements.addAll(task.getRequirements());
        tasks.add(task);
    }

    /**
     * Adds triggers to this environment.
     */
    public Environment triggers(@NotNull Trigger<?, ?>... triggers) {
        checkNotNull("triggers", triggers);
        for (Trigger<?, ?> t : triggers) {
            TriggerProperties trigger = EntityPropertiesBuilders.build(t);
            if (!trigger.applicableTo().contains(Applicability.DEPLOYMENTS)) {
                throw new PropertiesValidationException("Trigger " + t.getClass().getSimpleName() + " is not available in deployments");
            } else {
                this.triggers.add(trigger);
            }
        }
        return this;
    }

    /**
     * Adds deployment environment variables.
     */
    public Environment variables(@NotNull Variable... variables) {
        checkNotNull("variables", variables);
        Arrays.stream(variables)
                .map(EntityPropertiesBuilders::build)
                .forEach(this.variables::add);
        return this;
    }

    /**
     * Adds custom requirements to the environment.
     * <p>
     * Requirements control which Bamboo agents are able to execute deployments to the environment.
     */
    public Environment requirements(Requirement... requirements) {
        checkNotNull("requirements", requirements);
        Arrays.stream(requirements)
                .map(EntityPropertiesBuilders::build)
                .forEach(this.requirements::add);
        return this;
    }

    /**
     * Appends a notification rule to the environment.
     */
    public Environment notifications(@NotNull final Notification... notifications) {
        ImporterUtils.checkNotNull("notifications", notifications);
        Arrays.stream(notifications)
                .map(EntityPropertiesBuilders::build)
                .forEach(notification -> {
                    List<ValidationProblem> problems = new ArrayList<>();
                    if (!notification.getType().applicableTo().contains(Applicability.DEPLOYMENTS)) {
                        problems.add(new ValidationProblem(String.format("Can't add notification because notification type (%s) is not available in deployments",
                                notification.getType().getAtlassianPlugin().getCompleteModuleKey())));
                    }

                    for (NotificationRecipientProperties recipient : notification.getRecipients()) {
                        if (!recipient.applicableTo().contains(Applicability.DEPLOYMENTS)) {
                            problems.add(new ValidationProblem(String.format("Can't add notification because notification recipient (%s) is not available in deployments",
                                    recipient.getAtlassianPlugin().getCompleteModuleKey())));
                        }
                    }

                    if (!problems.isEmpty()) {
                        throw new PropertiesValidationException(problems);
                    } else {
                        this.notifications.add(notification);
                    }
                });
        return this;
    }

    /**
     * Configure Docker for this environment.
     *
     * @see DockerConfiguration
     */
    public Environment dockerConfiguration(@NotNull DockerConfiguration dockerConfiguration) {
        ImporterUtils.checkNotNull("dockerConfiguration", dockerConfiguration);
        this.dockerConfiguration = EntityPropertiesBuilders.build(dockerConfiguration);
        return this;
    }

    /**
     * Appends plugin configuration to the environment. If the same plugin is specified second time, its configuration
     * is overwritten.
     */
    public Environment pluginConfigurations(@NotNull final EnvironmentPluginConfiguration<? extends EnvironmentPluginConfigurationProperties>... pluginConfigurations) {
        ImporterUtils.checkNotNull("pluginConfigurations", pluginConfigurations);
        for (final EnvironmentPluginConfiguration<? extends EnvironmentPluginConfigurationProperties> pluginConfiguration : pluginConfigurations) {
            if (pluginConfiguration != null) {
                putPluginConfiguration(pluginConfiguration);
            }
        }
        return this;
    }

    private void putPluginConfiguration(final EnvironmentPluginConfiguration<?> pluginConfiguration) {
        EnvironmentPluginConfigurationProperties pluginConfigurationProperties = EntityPropertiesBuilders.build(pluginConfiguration);
        pluginConfigurations.put(pluginConfigurationProperties.getAtlassianPlugin().getCompleteModuleKey(), pluginConfigurationProperties);
    }

    @Override
    protected EnvironmentProperties build() {
        return new EnvironmentProperties(
                name,
                description,
                tasks,
                finalTasks,
                triggers,
                variables,
                requirements,
                notifications,
                dockerConfiguration,
                pluginConfigurations.values());
    }
}
