package com.atlassian.bamboo.specs.api.builders;


import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import org.jetbrains.annotations.NotNull;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

/**
 * Represents an identifier of any Atlassian plugin module.
 * <p>
 * Many pieces of functionality of Bamboo are implemented as Atlassian plugins. This class allows to find the correct
 * implementation amongst installed plugins.
 */
public class AtlassianModule extends EntityPropertiesBuilder<AtlassianModuleProperties> {
    private String completeModuleKey;

    /**
     * Specify an Atlassian plugin module with specific key.
     *
     * @param completeModuleKey plugin module key, as defined in respective atlassian-plguin.xml file
     */
    public AtlassianModule(@NotNull String completeModuleKey) {
        checkNotNull("completeModuleKey", completeModuleKey);

        this.completeModuleKey = completeModuleKey;
    }

    protected AtlassianModuleProperties build() {
        return new AtlassianModuleProperties(completeModuleKey);
    }
}
