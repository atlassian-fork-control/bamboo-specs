package com.atlassian.bamboo.specs.api.builders.project;

import com.atlassian.bamboo.specs.api.builders.BambooKey;
import com.atlassian.bamboo.specs.api.builders.BambooOid;
import com.atlassian.bamboo.specs.api.builders.EntityPropertiesBuilder;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.BambooKeyProperties;
import com.atlassian.bamboo.specs.api.model.BambooOidProperties;
import com.atlassian.bamboo.specs.api.model.project.ProjectProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.api.validators.common.ImporterUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Represents Bamboo project.
 */
public class Project extends EntityPropertiesBuilder<ProjectProperties> {
    private BambooKeyProperties key;
    private BambooOidProperties oid;
    private String name;
    private String description;

    /**
     * Specifies Bamboo project.
     */
    public Project() throws PropertiesValidationException {
    }

    /**
     * Sets a project name.
     */
    public Project name(@NotNull String name) throws PropertiesValidationException {
        ImporterUtils.checkNotNull("name", name);
        this.name = name;
        return this;
    }

    /**
     * Sets a project key. In the absence of oid key serves as project identifier.
     */
    public Project key(@Nullable String key) throws PropertiesValidationException {
        return key(key != null ? new BambooKey(key) : null);
    }

    /**
     * Sets a project key. In the absence of oid key serves as project identifier.
     */
    public Project key(@Nullable BambooKey key) throws PropertiesValidationException {

        this.key = key != null ? EntityPropertiesBuilders.build(key) : null;
        return this;
    }

    /**
     * Sets a project description.
     */
    public Project description(@Nullable String description) throws PropertiesValidationException {
        this.description = description;
        return this;
    }

    /**
     * Sets a project's oid.
     */
    public Project oid(@Nullable String oid) throws PropertiesValidationException {
        return oid(oid != null ? new BambooOid(oid) : null);
    }

    /**
     * Sets a project's oid.
     */
    public Project oid(@Nullable BambooOid oid) throws PropertiesValidationException {
        this.oid = oid != null ? EntityPropertiesBuilders.build(oid) : null;
        return this;
    }

    protected ProjectProperties build() throws PropertiesValidationException {
        return new ProjectProperties(
                oid,
                key,
                name,
                description);
    }
}
