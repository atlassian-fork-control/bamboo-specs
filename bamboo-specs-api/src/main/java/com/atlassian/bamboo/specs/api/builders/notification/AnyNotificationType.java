package com.atlassian.bamboo.specs.api.builders.notification;

import com.atlassian.bamboo.specs.api.builders.AtlassianModule;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.notification.AnyNotificationTypeProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import org.jetbrains.annotations.NotNull;

/**
 * Represents any notification type.
 * <p>
 * Since knowledge of internal representation of plugin data is required to properly construct this object,
 * this class should only be used if the specialised implementation is not available.
 */
public class AnyNotificationType extends NotificationType<AnyNotificationType, AnyNotificationTypeProperties> {

    private AtlassianModuleProperties atlassianPlugin;
    private String conditionString;


    /**
     * Specifies a notification condition of a given type.
     *
     * @param atlassianPlugin type of the notification condition identified by its plugin module key
     * @see AtlassianModule
     */
    public AnyNotificationType(@NotNull final AtlassianModule atlassianPlugin) throws PropertiesValidationException {
        this.atlassianPlugin = EntityPropertiesBuilders.build(atlassianPlugin);
    }

    /**
     * Sets configuration string for this condition.
     * <p>
     * The configuration should be in the format used by respective plugin. No syntactical nor semantic validation is
     * performed on the source data. The configuration is stored 'as is' in the Bamboo DB.
     */
    public AnyNotificationType conditionString(final String conditionString) {
        this.conditionString = conditionString;
        return this;
    }

    @NotNull
    @Override
    protected AnyNotificationTypeProperties build() {
        return new AnyNotificationTypeProperties(atlassianPlugin, conditionString);
    }
}
