/**
 * Items stored in environment custom plugins configuration.
 */
package com.atlassian.bamboo.specs.api.builders.deployment.configuration;
