package com.atlassian.bamboo.specs.api.builders.notification;

import com.atlassian.bamboo.specs.api.builders.AtlassianModule;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.notification.AnyNotificationRecipientProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import org.jetbrains.annotations.NotNull;

/**
 * Represents any type of notification recipient.
 * <p>
 * Since knowledge of internal representation of plugin data is required to properly construct this object,
 * this class should only be used if the specialised implementation is not available.
 */
public class AnyNotificationRecipient extends NotificationRecipient<AnyNotificationRecipient, AnyNotificationRecipientProperties> {

    private AtlassianModuleProperties atlassianPlugin;
    private String recipientString;


    /**
     * Specifies a notification recipient of a given type.
     *
     * @param atlassianPlugin type of the notification condition identified by its plugin module key
     * @see AtlassianModule
     */
    public AnyNotificationRecipient(@NotNull final AtlassianModule atlassianPlugin) throws PropertiesValidationException {
        this.atlassianPlugin = EntityPropertiesBuilders.build(atlassianPlugin);
    }

    /**
     * Sets configuration string for this recipient.
     * <p>
     * The configuration should be in the format used by respective plugin. No syntactical nor semantic validation is
     * performed on the source data. The configuration is stored 'as is' in the Bamboo DB.
     */
    public AnyNotificationRecipient recipientString(final String recipientString) {
        this.recipientString = recipientString;
        return this;
    }

    @NotNull
    @Override
    protected AnyNotificationRecipientProperties build() {
        return new AnyNotificationRecipientProperties(atlassianPlugin, recipientString);
    }
}
