/**
 * Items stored in environment custom plugins configuration.
 */
package com.atlassian.bamboo.specs.api.model.deployment.configuration;
