package com.atlassian.bamboo.specs.api.builders.permission;

public enum PermissionType {
    VIEW,
    EDIT,
    CREATE,
    DELETE,
    BUILD,
    CLONE,
    ADMIN
}
