package com.atlassian.bamboo.specs.api.validators.plan.branch;

import com.atlassian.bamboo.specs.api.model.plan.branches.CreatePlanBranchesProperties;
import com.atlassian.bamboo.specs.api.validators.common.ValidationProblem;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

public final class BranchCreationValidator {
    private BranchCreationValidator() {
    }

    @NotNull
    public static List<ValidationProblem> validate(@NotNull final CreatePlanBranchesProperties branchCreationProperties) {
        final List<ValidationProblem> errors = new ArrayList<>();

        if (branchCreationProperties.getTrigger() == CreatePlanBranchesProperties.Trigger.BRANCH) {
            try {
                //noinspection ResultOfMethodCallIgnored
                Pattern.compile(branchCreationProperties.getMatchingPattern());
            } catch (PatternSyntaxException e) {
                errors.add(new ValidationProblem("Branch name pattern is not correct regex: " + e.getMessage()));
            }
        }

        return errors;
    }
}
