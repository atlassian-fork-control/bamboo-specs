package com.atlassian.bamboo.specs.api.model.plan.condition;

import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.EntityProperties;

public interface ConditionProperties extends EntityProperties {
    AtlassianModuleProperties getAtlassianPlugin();
}
