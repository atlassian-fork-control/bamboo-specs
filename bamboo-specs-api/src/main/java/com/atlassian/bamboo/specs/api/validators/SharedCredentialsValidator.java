package com.atlassian.bamboo.specs.api.validators;

import com.atlassian.bamboo.specs.api.model.credentials.SharedCredentialsProperties;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import com.atlassian.bamboo.specs.api.validators.common.ValidationProblem;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import static com.atlassian.bamboo.specs.api.validators.common.ValidationUtils.validateName;

public final class SharedCredentialsValidator {
    private SharedCredentialsValidator() {
    }

    public static List<ValidationProblem> validate(@NotNull final SharedCredentialsProperties sharedCredentialsProperties) {
        final ValidationContext context = ValidationContext.of("Shared credentials");
        final List<ValidationProblem> errors = new ArrayList<>();

        errors.addAll(validateName(context, sharedCredentialsProperties.getName()));

        return errors;
    }
}
