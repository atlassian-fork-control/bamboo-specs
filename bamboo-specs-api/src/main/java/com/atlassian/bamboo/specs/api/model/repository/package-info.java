/**
 * Generic plan-local and linked source code repositories as well as change detection settings.
 */
package com.atlassian.bamboo.specs.api.model.repository;
