package com.atlassian.bamboo.specs.api.model.deployment.configuration;

import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;

import javax.annotation.concurrent.Immutable;
import java.util.Collections;
import java.util.Map;
import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

@Immutable
public class AnyPluginConfigurationProperties implements EnvironmentPluginConfigurationProperties {
    private final AtlassianModuleProperties atlassianPlugin;
    private final Map<String, String> configuration;

    private AnyPluginConfigurationProperties() {
        atlassianPlugin = null;
        configuration = Collections.emptyMap();
    }

    public AnyPluginConfigurationProperties(AtlassianModuleProperties atlassianPlugin, Map<String, String> configuration) {
        this.atlassianPlugin = atlassianPlugin;
        this.configuration = configuration;
    }

    @Override
    public AtlassianModuleProperties getAtlassianPlugin() {
        return atlassianPlugin;
    }

    public Map<String, String> getConfiguration() {
        return configuration;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AnyPluginConfigurationProperties that = (AnyPluginConfigurationProperties) o;
        return Objects.equals(getAtlassianPlugin(), that.getAtlassianPlugin()) &&
                Objects.equals(getConfiguration(), that.getConfiguration());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getAtlassianPlugin(), getConfiguration());
    }

    @Override
    public void validate() {
        checkNotNull("atlassianPlugin", atlassianPlugin);
    }
}
