package com.atlassian.bamboo.specs.api.model.trigger;

import com.atlassian.bamboo.specs.api.builders.trigger.RepositoryBasedTrigger.TriggeringRepositoriesType;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.repository.VcsRepositoryIdentifierProperties;
import org.jetbrains.annotations.NotNull;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

public final class AnyTriggerProperties extends RepositoryBasedTriggerProperties {
    private final AtlassianModuleProperties atlassianPlugin;
    private final Map<String, String> configuration;

    private AnyTriggerProperties() {
        super();
        atlassianPlugin = null;
        configuration = Collections.emptyMap();
    }

    public AnyTriggerProperties(@NotNull final AtlassianModuleProperties atlassianPlugin,
                                final String name,
                                final String description,
                                final boolean isEnabled,
                                final Map<String, String> configuration,
                                final TriggeringRepositoriesType triggeringRepositoriesType,
                                final List<VcsRepositoryIdentifierProperties> selectedTriggeringRepositories) throws PropertiesValidationException {
        super(name, description, isEnabled, triggeringRepositoriesType, selectedTriggeringRepositories);
        this.atlassianPlugin = atlassianPlugin;
        this.configuration = Collections.unmodifiableMap(new LinkedHashMap<>(configuration));
        validate();
    }

    @NotNull
    @Override
    public AtlassianModuleProperties getAtlassianPlugin() {
        return atlassianPlugin;
    }

    @NotNull
    public Map<String, String> getConfiguration() {
        return configuration;
    }

    @Override
    public void validate() {
        super.validate();
        checkNotNull("atlassianPlugin", atlassianPlugin);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        AnyTriggerProperties that = (AnyTriggerProperties) o;
        return Objects.equals(atlassianPlugin, that.atlassianPlugin) &&
                Objects.equals(configuration, that.configuration);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), atlassianPlugin, configuration);
    }
}
