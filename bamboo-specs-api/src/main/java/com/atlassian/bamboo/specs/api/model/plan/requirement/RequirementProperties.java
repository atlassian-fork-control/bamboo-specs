package com.atlassian.bamboo.specs.api.model.plan.requirement;

import com.atlassian.bamboo.specs.api.builders.requirement.Requirement;
import com.atlassian.bamboo.specs.api.builders.requirement.Requirement.MatchType;
import com.atlassian.bamboo.specs.api.codegen.annotations.Builder;
import com.atlassian.bamboo.specs.api.codegen.annotations.ConstructFrom;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.EntityProperties;
import com.atlassian.bamboo.specs.api.validators.common.ImporterUtils;

import javax.annotation.concurrent.Immutable;
import java.util.Objects;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

@Immutable
@Builder(Requirement.class)
@ConstructFrom({"key"})
public class RequirementProperties implements EntityProperties {

    private final String key;
    private final String matchValue;
    private final MatchType matchType;

    private RequirementProperties() {
        matchType = MatchType.EXISTS;
        matchValue = ".*";
        key = null;
    }

    public RequirementProperties(final String key, final String matchValue, final MatchType matchType) {
        this.key = key;
        this.matchValue = matchValue;
        this.matchType = matchType;
        validate();
    }

    public String getKey() {
        return key;
    }

    public String getMatchValue() {
        return matchValue;
    }

    public MatchType getMatchType() {
        return matchType;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final RequirementProperties that = (RequirementProperties) o;
        return Objects.equals(getKey(), that.getKey()) &&
                Objects.equals(getMatchValue(), that.getMatchValue()) &&
                getMatchType() == that.getMatchType();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getKey(), getMatchValue(), getMatchType());
    }

    public void validate() throws PropertiesValidationException {
        ImporterUtils.checkNotBlank("key", key);
        ImporterUtils.checkNotNull("matchType", matchType);
        if (matchType != MatchType.EXISTS) {
            ImporterUtils.checkNotBlank("matchValue", matchValue);
        }
        if (matchType == MatchType.MATCHES) {
            try {
                Pattern.compile(matchValue);
            } catch (PatternSyntaxException e) {
                throw new PropertiesValidationException("Branch name pattern is not correct regex: " + e.getMessage());
            }
        }
    }
}
