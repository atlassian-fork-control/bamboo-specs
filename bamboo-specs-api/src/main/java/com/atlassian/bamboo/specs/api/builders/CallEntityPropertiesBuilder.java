package com.atlassian.bamboo.specs.api.builders;

import com.atlassian.bamboo.specs.api.model.EntityProperties;

/**
 * Used to call protected  {@link EntityPropertiesBuilder#build()}.
 */
public abstract class CallEntityPropertiesBuilder<T extends EntityProperties> extends EntityPropertiesBuilder<T> {
    public static <T extends EntityProperties> T build(final EntityPropertiesBuilder<T> e) {
        return e.build();
    }
}
