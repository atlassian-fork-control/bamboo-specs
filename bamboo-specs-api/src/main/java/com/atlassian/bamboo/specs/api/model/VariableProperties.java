package com.atlassian.bamboo.specs.api.model;

import com.atlassian.bamboo.specs.api.codegen.annotations.ConstructFrom;
import com.atlassian.bamboo.specs.api.codegen.annotations.NoArgCallForBooleanValue;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.validators.VariableValidator;
import org.jetbrains.annotations.NotNull;

import javax.annotation.concurrent.Immutable;
import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNoErrors;
import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

@Immutable
@ConstructFrom({"name", "value"})
public final class VariableProperties implements EntityProperties {
    public final String name;
    public final String value;
    @NoArgCallForBooleanValue
    public final boolean createOnly;

    private VariableProperties() {
        name = null;
        value = null;
        createOnly = false;
    }

    public VariableProperties(@NotNull String name, @NotNull String value, boolean createOnly) throws PropertiesValidationException {
        this.name = name;
        this.value = value;
        this.createOnly = createOnly;

        validate();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        VariableProperties that = (VariableProperties) o;
        return Objects.equals(getName(), that.getName()) &&
                Objects.equals(getValue(), that.getValue()) &&
                Objects.equals(isCreateOnly(), that.isCreateOnly());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getValue(), isCreateOnly());
    }

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }

    public boolean isCreateOnly() {
        return createOnly;
    }

    @Override
    public void validate() {
        checkNotNull("name", name);
        checkNotNull("value", value);
        checkNoErrors(VariableValidator.validate(this));
    }
}
