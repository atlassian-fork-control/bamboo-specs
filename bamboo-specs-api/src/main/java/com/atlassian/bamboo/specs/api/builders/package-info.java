/**
 * <b>The 'builders.*' packages contain classes you can use to define your Bamboo configuration as code, see also com.atlassian.bamboo.specs.builders.</b>
 */
package com.atlassian.bamboo.specs.api.builders;
