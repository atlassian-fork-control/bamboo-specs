package com.atlassian.bamboo.specs.api.model.plan.branches;

import com.atlassian.bamboo.specs.api.codegen.annotations.CodeGeneratorName;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.EntityProperties;
import com.atlassian.bamboo.specs.api.validators.plan.branch.BranchCleanupValidator;
import org.jetbrains.annotations.NotNull;

import javax.annotation.concurrent.Immutable;
import java.time.Duration;
import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNoErrors;

@Immutable
@CodeGeneratorName("com.atlassian.bamboo.specs.codegen.emitters.plan.branches.BranchCleanupEmitter")
public class BranchCleanupProperties implements EntityProperties {
    private boolean removeDeletedFromRepository = true;
    private boolean removeInactiveInRepository = true;
    private Duration removeDeletedFromRepositoryPeriod = Duration.ZERO;
    private Duration removeInactiveInRepositoryPeriod = Duration.ZERO;

    @SuppressWarnings("unused")
    private BranchCleanupProperties() {
    }

    public BranchCleanupProperties(final boolean removeDeletedFromRepository,
                                   @NotNull final Duration removeDeletedFromRepositoryPeriod, final boolean removeInactiveInRepository,
                                   @NotNull final Duration removeInactiveInRepositoryPeriod) throws PropertiesValidationException {
        this.removeDeletedFromRepository = removeDeletedFromRepository;
        this.removeDeletedFromRepositoryPeriod = removeDeletedFromRepositoryPeriod;

        this.removeInactiveInRepository = removeInactiveInRepository;
        this.removeInactiveInRepositoryPeriod = removeInactiveInRepositoryPeriod;
        validate();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        BranchCleanupProperties that = (BranchCleanupProperties) o;
        return isRemoveDeletedFromRepository() == that.isRemoveDeletedFromRepository() &&
                isRemoveInactiveInRepository() == that.isRemoveInactiveInRepository() &&
                Objects.equals(getRemoveInactiveInRepositoryPeriod(), that.getRemoveInactiveInRepositoryPeriod()) &&
                Objects.equals(getRemoveDeletedFromRepositoryPeriod(), that.getRemoveDeletedFromRepositoryPeriod());
    }

    @Override
    public int hashCode() {
        return Objects.hash(isRemoveDeletedFromRepository(), isRemoveInactiveInRepository(), getRemoveDeletedFromRepositoryPeriod(), getRemoveInactiveInRepositoryPeriod());
    }

    public boolean isRemoveDeletedFromRepository() {
        return removeDeletedFromRepository;
    }

    public boolean isRemoveInactiveInRepository() {
        return removeInactiveInRepository;
    }

    public Duration getRemoveDeletedFromRepositoryPeriod() {
        return removeDeletedFromRepositoryPeriod;
    }

    public Duration getRemoveInactiveInRepositoryPeriod() {
        return removeInactiveInRepositoryPeriod;
    }

    @Override
    public void validate() {
        checkNoErrors(BranchCleanupValidator.validate(this));
    }
}
