package com.atlassian.bamboo.specs.api.builders.permission;

import com.atlassian.bamboo.specs.api.builders.BambooOid;
import com.atlassian.bamboo.specs.api.builders.RootEntityPropertiesBuilder;
import com.atlassian.bamboo.specs.api.builders.deployment.Deployment;
import com.atlassian.bamboo.specs.api.model.permission.EnvironmentPermissionsProperties;
import com.atlassian.bamboo.specs.api.model.permission.PermissionsProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import org.jetbrains.annotations.NotNull;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotBlank;
import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

/**
 * Entity representing permissions for deployment environments.
 * Note that this object needs to be published separately from {@link Deployment} environment belongs to.
 * Pre-existing permissions that are not defined in associated {@link Permissions} object are <b>revoked</b> when this object is published.
 */
public class EnvironmentPermissions extends RootEntityPropertiesBuilder<EnvironmentPermissionsProperties> {

    public static final String TYPE = "environment permission";

    private BambooOid deploymentOid;
    private String deploymentName;
    private String environmentName;
    private Permissions permissions;

    @Override
    protected EnvironmentPermissionsProperties build() {
        final PermissionsProperties permissionProperties = permissions != null
                ? EntityPropertiesBuilders.build(permissions)
                : null;
        if (deploymentOid != null) {
            return new EnvironmentPermissionsProperties(EntityPropertiesBuilders.build(deploymentOid), environmentName, permissionProperties);
        } else {
            return new EnvironmentPermissionsProperties(deploymentName, environmentName, permissionProperties);
        }
    }

    public EnvironmentPermissions(final BambooOid deploymentOid) {
        checkNotNull("deploymentOid", deploymentOid);
        this.deploymentOid = deploymentOid;
    }

    public EnvironmentPermissions(final String deploymentProjectName) {
        checkNotBlank("deployment project name", deploymentProjectName);
        this.deploymentName = deploymentProjectName;
    }

    public EnvironmentPermissions(final String deploymentProjectName, final String environmentName) {
        checkNotBlank("deployment project name", deploymentProjectName);
        checkNotBlank("environment name", environmentName);
        this.deploymentName = deploymentProjectName;
        this.environmentName = environmentName;
    }

    public EnvironmentPermissions deploymentProjectName(final String deploymentProjectName) {
        checkNotBlank("deployment project name", deploymentProjectName);
        this.deploymentName = deploymentProjectName;
        return this;
    }

    public EnvironmentPermissions environmentName(@NotNull final String environmentName) {
        checkNotBlank("environment name", environmentName);
        this.environmentName = environmentName;
        return this;
    }

    public EnvironmentPermissions permissions(final Permissions permissions) {
        checkNotNull("permissions", permissions);
        this.permissions = permissions;
        return this;
    }

    public String getDeploymentName() {
        return deploymentName;
    }

    public String getEnvironmentName() {
        return environmentName;
    }

    @Override
    public String humanReadableType() {
        return TYPE;
    }

    @Override
    public String humanReadableId() {
        final StringBuilder id = new StringBuilder();
        id.append(TYPE);
        id.append(" for deployment ");
        if (deploymentOid != null) {
            id.append(deploymentOid);
        } else {
            id.append(deploymentName);
        }
        id.append(" environment ");
        id.append(environmentName);
        return id.toString();
    }
}
