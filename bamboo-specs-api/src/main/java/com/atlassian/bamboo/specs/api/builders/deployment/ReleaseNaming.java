package com.atlassian.bamboo.specs.api.builders.deployment;

import com.atlassian.bamboo.specs.api.builders.EntityPropertiesBuilder;
import com.atlassian.bamboo.specs.api.model.deployment.ReleaseNamingProperties;
import com.atlassian.bamboo.specs.api.validators.common.ImporterUtils;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

/**
 * Specifies release naming scheme of a deployment project, that is, defines what version name Bamboo should assign to automatically created releases.
 * You can override this manually whenever you create a new release.
 */
public class ReleaseNaming extends EntityPropertiesBuilder<ReleaseNamingProperties> {
    private String nextVersionName;
    private boolean autoIncrement;
    private boolean applicableToBranches;
    private Set<String> variablesToAutoIncrement = new HashSet<>();

    /**
     * Specifies the name of the next release. The name can contain references to variables, e.g. "myRelease-${bamboo.buildNumber}".
     * If there's a naming conflict during automatic creation of a release, Bamboo will suffix provided name with a number.
     * <p>
     * If automatic incrementation of release number is on, the system will prevent the incremented part of the name from being accidentally
     * reverted to a previous value when publishing the specs. See also: {@link #autoIncrement(boolean)}.
     * </p>
     *
     * @param nextVersionName name of the next release
     */
    public ReleaseNaming(@NotNull String nextVersionName) {
        ImporterUtils.checkNotBlank("nextVersionName", nextVersionName);
        this.nextVersionName = nextVersionName;
    }

    /**
     * Specifies if Bamboo should automatically increment numeric part of release name.
     * <p>
     * Bamboo can automatically increment the last numeric component of the release name once the new release is created.
     * For instance: if next release name is set to "release-1.2.3", Bamboo will set it to "release-1.2.4" after a release is created.
     * If this option is on, Bamboo Specs engine prevents reverting the incremented part of the release name. Using the above example: setting
     * next release name to "release-1.2.2" or "release-1.2.1" in Bamboo Specs has no effect, but setting it to "release-1.2.5" or "release-2.2.1" does.
     * </p>
     *
     * @param autoIncrement true if Bamboo should increment release names automatically
     */
    public ReleaseNaming autoIncrement(boolean autoIncrement) {
        this.autoIncrement = autoIncrement;
        return this;
    }

    /**
     * Specifies if this naming scheme should be applied to releases created from plan branches.
     * If the option is on, all the rules defined by this object are in effect when creating releases from plan branches.
     * If the option is off, releases from branches will default to using the branch name suffixed with the build number of the build result and automatic
     * incrementation of variables and release number is not performed.
     */
    public ReleaseNaming applicableToBranches(boolean applicableToBranches) {
        this.applicableToBranches = applicableToBranches;
        return this;
    }

    /**
     * Specifies which variables referenced in release name should be incremented after creating a release.
     * The variables must be defined at either global or plan scope.
     * If variable is defined at both plan and global level, the plan variable is the one being incremented.
     * If {@link #applicableToBranches(boolean)} option is on, a release is created from a branch and the variable is overridden for that branch,
     * then the value associated with the branch is incremented.
     */
    public ReleaseNaming variablesToAutoIncrement(@NotNull String... variablesToAutoIncrement) {
        checkNotNull("variablesToAutoIncrement", variablesToAutoIncrement);
        this.variablesToAutoIncrement.addAll(Arrays.asList(variablesToAutoIncrement));
        return this;
    }

    protected ReleaseNamingProperties build() {
        return new ReleaseNamingProperties(nextVersionName, autoIncrement, applicableToBranches, variablesToAutoIncrement);
    }
}
