package com.atlassian.bamboo.specs.api.util;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class VariableUtils {
    /**
     * Bamboo variable usage pattern. Variable usage consists of variable name, preceded by the namespace (usually
     * "bamboo"), put between "${" and "}".
     */
    private static final Pattern VARIABLE_USAGE_PATTERN = Pattern.compile("\\$\\{([a-z]+)\\.([^\\$\\{\\}]+)\\}");

    /**
     * List of fields which require masking/encrypting.
     */
    private static final String[] PASSWORD_FIELD_NAMES = {"password", "sshKey", "secret", "passphrase"};


    private VariableUtils() {
    }

    /**
     * <p>Returns true if the given text contains variable usages from any namespace.
     * </p>
     * <p>Variable usage consists of variable name, preceded by the namespace (usually "bamboo"), put between "${" and
     * "}".
     * </p>
     * Example variable usages:
     * <pre>{@code
     * - ${bamboo.my.custom.variable}
     * - ${bamboo.functionalTestOptions}
     * - ${system.M2_HOME}
     * }</pre>
     *
     * @param text text to verify
     * @return true, if text contains variable usages, false otherwise
     */
    public static boolean containsVariableUsage(@NotNull String text) {
        return VARIABLE_USAGE_PATTERN.matcher(text).find();
    }

    public static class VariableUsage {
        private final String namespace;
        private final String name;

        public VariableUsage(String namespace, String name) {
            this.namespace = namespace;
            this.name = name;
        }

        public String getNamespace() {
            return namespace;
        }

        public String getName() {
            return name;
        }
    }

    /**
     * Finds all variable usages within a given text.
     */
    @NotNull
    public static List<VariableUsage> findVariableUsages(@NotNull String text) {
        final Matcher matcher = VARIABLE_USAGE_PATTERN.matcher(text);
        final List<VariableUsage> result = new ArrayList<>();
        while (matcher.find()) {
            result.add(new VariableUsage(matcher.group(1), matcher.group(2)));
        }
        return result;
    }

    public static boolean isPasswordVariable(@NotNull String variableKey) {
        for (String pattern : PASSWORD_FIELD_NAMES) {
            if (StringUtils.containsIgnoreCase(variableKey, pattern)) {
                return true;
            }
        }
        return false;
    }
}
