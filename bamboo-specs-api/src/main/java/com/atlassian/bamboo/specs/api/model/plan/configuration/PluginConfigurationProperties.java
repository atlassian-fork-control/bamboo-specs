package com.atlassian.bamboo.specs.api.model.plan.configuration;

import com.atlassian.bamboo.specs.api.model.AtlassianModuleProperties;
import com.atlassian.bamboo.specs.api.model.EntityProperties;

public interface PluginConfigurationProperties extends EntityProperties {
    AtlassianModuleProperties getAtlassianPlugin();
}
