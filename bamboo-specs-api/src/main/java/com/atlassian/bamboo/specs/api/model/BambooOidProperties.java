package com.atlassian.bamboo.specs.api.model;


import com.atlassian.bamboo.specs.api.codegen.annotations.ConstructFrom;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.validators.BambooOidValidator;
import org.jetbrains.annotations.NotNull;

import javax.annotation.concurrent.Immutable;
import java.util.Objects;

import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNoErrors;
import static com.atlassian.bamboo.specs.api.validators.common.ImporterUtils.checkNotNull;

@ConstructFrom({"oid"})
@Immutable
public final class BambooOidProperties implements EntityProperties {
    private final String oid;

    private BambooOidProperties() {
        oid = null;
    }

    public BambooOidProperties(@NotNull final String oid) throws PropertiesValidationException {
        this.oid = oid;

        validate();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        BambooOidProperties that = (BambooOidProperties) o;
        return Objects.equals(getOid(), that.getOid());
    }

    @Override
    public String toString() {
        return oid;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getOid());
    }

    @NotNull
    public String getOid() {
        return oid;
    }

    @Override
    public void validate() {
        checkNotNull("oid", oid);
        checkNoErrors(BambooOidValidator.validate(this));
    }
}
