/**
 * Exceptions thrown by Bamboo Specs library.
 */
package com.atlassian.bamboo.specs.api.exceptions;
