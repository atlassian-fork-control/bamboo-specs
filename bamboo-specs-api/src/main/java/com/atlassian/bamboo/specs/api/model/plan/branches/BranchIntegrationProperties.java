package com.atlassian.bamboo.specs.api.model.plan.branches;


import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.EntityProperties;
import com.atlassian.bamboo.specs.api.model.plan.PlanBranchIdentifierProperties;
import org.jetbrains.annotations.Nullable;

import javax.annotation.concurrent.Immutable;
import java.util.Objects;

@Immutable
public final class BranchIntegrationProperties implements EntityProperties {
    private boolean enabled = true;
    private PlanBranchIdentifierProperties integrationBranch;
    private boolean gatekeeper;
    private boolean pushOn;

    private BranchIntegrationProperties() {
    }

    public BranchIntegrationProperties(final boolean enabled,
                                       @Nullable final PlanBranchIdentifierProperties integrationBranch,
                                       final boolean gatekeeper,
                                       final boolean pushOn) throws PropertiesValidationException {
        this.enabled = enabled;
        this.integrationBranch = integrationBranch;
        this.gatekeeper = gatekeeper;
        this.pushOn = pushOn;

        validate();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        BranchIntegrationProperties that = (BranchIntegrationProperties) o;
        return isEnabled() == that.isEnabled() &&
                isGatekeeper() == that.isGatekeeper() &&
                isPushOn() == that.isPushOn() &&
                Objects.equals(getIntegrationBranch(), that.getIntegrationBranch());
    }

    @Override
    public int hashCode() {
        return Objects.hash(isEnabled(), getIntegrationBranch(), isGatekeeper(), isPushOn());
    }

    public boolean isEnabled() {
        return enabled;
    }

    @Nullable
    public PlanBranchIdentifierProperties getIntegrationBranch() {
        return integrationBranch;
    }

    public boolean isGatekeeper() {
        return gatekeeper;
    }

    public boolean isPushOn() {
        return pushOn;
    }

    @Override
    public void validate() {
    }
}
