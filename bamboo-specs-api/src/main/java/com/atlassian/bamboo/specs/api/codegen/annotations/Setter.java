package com.atlassian.bamboo.specs.api.codegen.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Names setter method for a field in entity properties.
 * This annotation should be used if name of the setter method in builder does not match name of the field.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Setter {
    String value();
}
