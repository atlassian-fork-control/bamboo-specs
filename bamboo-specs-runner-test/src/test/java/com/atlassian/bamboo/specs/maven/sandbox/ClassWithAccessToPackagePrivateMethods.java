package com.atlassian.bamboo.specs.maven.sandbox;

public class ClassWithAccessToPackagePrivateMethods {
    public ClassWithAccessToPackagePrivateMethods() {
        PrivilegedThreads.resetThreadPermissionCheckers();
    }
}
